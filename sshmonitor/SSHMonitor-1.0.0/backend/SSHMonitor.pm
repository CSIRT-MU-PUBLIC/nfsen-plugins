#!/usr/bin/perl
#
# dict.pm -  NfSen plugin for massive brute force attack detection 
# 
# Copyright (C) 2010-2013, Masaryk University
# Authors: Jan VYKOPAL <vykopal@ics.muni.cz>
#          Martin VIZVARY, <vizvary@ics.muni.cz>
#         
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in
#    the documentation and/or other materials provided with the
#    distribution.
# 3. Neither the name of the Masaryk University nor the names of its
#    contributors may be used to endorse or promote products derived from
#     this software without specific prior written permission.
#
# This software is provided ``as is'', and any express or implied
# warranties, including, but not limited to, the implied warranties of
# merchantability and fitness for a particular purpose are disclaimed.
# In no event shall the Masaryk University or contributors be liable for
# any direct, indirect, incidental, special, exemplary, or consequential
# damages (including, but not limited to, procurement of substitute
# goods or services; loss of use, data, or profits; or business
# interruption) however caused and on any theory of liability, whether
# in contract, strict liability, or tort (including negligence or
# otherwise) arising in any way out of the use of this software, even
# if advised of the possibility of such damage.
#
# Version: 1.0.0 beta
#

package SSHMonitor;

# highly recommended for good style Perl programming
use strict;
use warnings;

# Link used modules
#NfSen related modules
use NfProfile;
use NfConf;

#Postgres DB related modules
use DBI;

#other modules
use IP::Country::Fast;
use Socket;
use Sys::Syslog;
use Email::Simple;
use POSIX;
use Net::CIDR;
use DateTime;
use DateTime::TimeZone;
use DateTime::Format::Strptime;
use Storable;
use URI::Escape;

#use FindBin;

#Profiling related module
use Time::HiRes;

our $VERSION = 100;

#SET TO 1 FOR TIME PROFILING (OUTPUT TO SYSLOG) FOR ALL COMPLEX FUNCTIONS
our $PROFILING = 0;
our $TEST = 0;

#Frontend calls
our %cmd_lookup = (
	"feed_graph" => \&FeedGraph,
	"get_pg_data"  => \&GetDataFromPg,
	"save_settings" => \&SaveSettings,
	"load_settings" => \&LoadSettings,
);

#global variables initialized in Init function
my ($NFDUMP, $PROFILEDIR, $MAIL_TO, $MAIL_FROM, $CHANNEL_IN, $CHANNEL_OUT, $DB_HOST,
    $DB_PORT, $DB_NAME, $DB_USER, $DB_PASSWD, @WHITELIST, $TIMEZONE, $NFCONF,$LOCAL_NET, 
    $INTERVAL, $CHANNEL_SCAN, $SQLITE, $SQLITE_PATH);


if($SQLITE){
  require DBD::SQLite;
} else {
  require DBD::Pg;
}

=item
 LoadSettings - load plugin settings at initialization from nfsen.conf file. Send the config hashref 
 to frontend.
 
 Input: --
 Output: send nfconf through socket
=cut
sub LoadSettings 
{
  my $loadSettingsTime = Time::HiRes::time() if ($PROFILING);
  my $socket = shift;#scalar
  my $opts = shift;#hashref

  if($PROFILING){
    my $loadSettingsTotal = sprintf("%.6f", Time::HiRes::time() - $loadSettingsTime);
    syslog("info", "SSHMonitor: PROFILING: LoadSettings: $loadSettingsTotal sec.");
  }

# Sent settings to frontend
  Nfcomm::socket_send_ok ($socket, \%$NFCONF);
  return(1);
} # End of LoadSettings

=item
 SaveSettings - save email text to BACKEND_PLUGINDIR/dict/email.txt. 
 
 Input: $socket - nfsen socket
        $text - email text
 Output: send nfconf through socket
=cut
sub SaveSettings {
  my $socket = shift;
  my $opts = shift;
  syslog("info", "SSHMonitor: Save Settings");
  # Initialize the name of settings file
  my $path = $NfConf::BACKEND_PLUGINDIR;
  my $file = "$path/SSHMonitor/email.txt";
  my %args;
  $args{'settings_saved'} = "1";
  # Save settings to the file
  if(!open(my $file_handle, ">", $file)){
    syslog("info", "SSHMonitor: Failed to open email.txt $file: $!");
    $args{'settings_saved'} = 0;
  } else {
    my $text = uri_unescape($$opts{'text'});
    $text =~ s/\+/ /gx;
    print $file_handle $text;
    close($file_handle) or syslog("info", "SSHMonitor: Failed to close filehandler on email.txt: $file: $!") and $args{'settings_saved'} = 0;
  }

  # Send "1" to the frontend
  Nfcomm::socket_send_ok ($socket, \%args);
  return(1);
} # End of SaveSettings

=item
 ConnectToPg - connect to the PostgreSQL DB using settings in nfsen.conf
 
 Input: --
 Output:
   On success: (1, db_handler)
   On error: (0, 'error message')
=cut
sub ConnectToPg 
{ 
  my $connectToPgTime = Time::HiRes::time() if ($PROFILING);
   
  my $dsn = "dbi:Pg:database=$DB_NAME;host=$DB_HOST;port=$DB_PORT";
  my $dbh = DBI->connect($dsn, $DB_USER, $DB_PASSWD) or return(0, "Cannot connect to database: $dsn " . $DBI::errstr.", $DB_NAME, $DB_HOST, $DB_PORT");
  
  if($PROFILING){
    my $connectToPgTotal = sprintf("%.6f", Time::HiRes::time() -  $connectToPgTime);
    syslog("info", "SSHMonitor: PROFILING: ConnectToPg: $connectToPgTotal sec.");
  }
  return(1, $dbh);
} # end of ConnectToPg

=item
 ConnectToSQLite - connect to the SQLite DB using settings in nfsen.conf

 Input: --
 Output:
   On success: (1, db_handler)
   On error: (0, 'error message')
=cut
sub ConnectToSQLite
{
  my $connectToSQLiteTime = Time::HiRes::time() if ($PROFILING);


  my $dsn = "dbi:SQLite:dbname=$SQLITE_PATH";
  my $dbh = DBI->connect("$dsn","","") or return(0, "Cannot connect to database: $dsn " . $DBI::errstr.", $SQLITE_PATH");

  if($PROFILING){
    my $connectToSQLiteTotal = sprintf("%.6f", Time::HiRes::time() -  $connectToSQLiteTime);
    syslog("info", "sshMonitor: PROFILING: ConnectToSQLite: $connectToSQLiteTotal sec.");
  }
  return(1, $dbh);
} # end of ConnectToSQLite


=item
 CheckPgTable - create data tables in PostgreSQL if don't exists
 
 Input:
   On success: (1, 'string')
   On error: (0, 'error message')
=cut
sub CheckPgTable {
  my $createPgTableTime = Time::HiRes::time() if ($PROFILING);
 
  my ($rc, $result) = ConnectToPg();
  if(!$rc){
    return(0, "CheckPgTable failed: $result");
  }
  
  syslog("info", "SSHMonitor: connection to DB succesfull.");

  my $dbh = $result;
  #check if are there any table with given names
  my $sth = $dbh->prepare("SELECT tablename FROM pg_tables WHERE tablename='attacks' or tablename='scans' or tablename = 'ip_details' or tablename = 'stats_unique'");
  if(!defined $sth){ 
    return(0, "Cannot prepare statement: " . $dbh->errstr); 
  }
  $sth->execute or syslog("info", "SSHMonitor: Cannot execute statement: " . $dbh->errstr);
  #if there are not tables create them
  if(!$sth->fetchrow()){
    $sth->finish;
    return(0, "Tables are not prepared. Check your DB and create all tables specified in PostgreSQL file!");
  }  

  $sth->finish; 
  #disconnect is not critical - just warn 
  $dbh->disconnect() or warn("SSHMonitor: Disconnection from DB failed " . $dbh->errstr);

  #if profiling is 1 print time to syslog
  if($PROFILING){
    my $createPgTableTotal = sprintf("%.6f", Time::HiRes::time() -  $createPgTableTime);
    syslog("info", "RdpMonior: PROFILING: CreateTables: $createPgTableTotal sec.");
  }
  
  return(1, "Tables ready");
} # end of CreatePgTabe


=item
 GetDataFromPg - get data from PostgreSQL database. Statistics about attacks,
 scans and ip details.

 Which data will be sent to fronted depends on the value of "graph_name"
 option in $opts array.

 Input: $socket - nfsen socket, handle of communication socket to the frontend
        $opts - hashref, options sent from frontend
 Output: 
   On success: send data over socket
   On failure: (0, 'error message')
=cut
sub GetDataFromPg
{
  my $GetDataFromPgTime = Time::HiRes::time() if ($PROFILING);

  my $socket      = shift;    # scalar
  my $opts        = shift;    # hashref
  my $graph_name  = $opts->{"graph_name"};
  my $begin;
  my $end;
  eval {
    $begin = DateTime->from_epoch(epoch => $opts->{"begin"}, time_zone => 'local');
    $end   = DateTime->from_epoch(epoch => $opts->{"end"}, time_zone => 'local');
  };

  syslog("info", "SSHMonitor plugin run: $@") if $@;
  
  my $rc;
  my $result;

  if($SQLITE){
    ($rc, $result) = ConnectToSQLite();
  } else {
    ($rc, $result) = ConnectToPg();
  }

  if(!$rc){
    return(0, "SSHMonitor: DB connection failed: $result");
  }
  #DB handler
  my $dbh = $result;

  my $sth;
  my %args;

  if((!defined $opts->{limit}) or ($opts->{limit} eq '')){
    $opts->{limit} = 100;
  }

  #get data for Attacks
  if($graph_name eq 'highchart_attacks'){
    my $query;
    if(!$SQLITE){
      $query = "SELECT ts,sip,name,country,dip,flows FROM (SELECT * FROM attacks WHERE ts >= ? AND ts <= ?";
    }else{
      $query = "SELECT ts,sip,name,country,dip,flows FROM (SELECT ts as ts,sip as sip,dip as dip,flows as flows FROM attacks WHERE datetime(ts) >= datetime(?) AND datetime(ts) <= datetime(?)";
    }

    if($opts->{src_ip} =~ m/\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}/){
      $query .= " and sip = '".$opts->{src_ip}."'";
    }
    if($opts->{dst_ip} =~ m/\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}/){
      $query .= " and dip = '".$opts->{dst_ip}."'";
    }
    if(!$SQLITE){
      $query .= ") AS A INNER JOIN (SELECT DISTINCT(ip),name,country from ip_details WHERE date(ts) >= ? and ts <= ?) AS B On a.sip = b.ip ORDER BY ts DESC LIMIT " . $opts->{limit} . ";";
    }else{
      $query .= ") AS A INNER JOIN (SELECT DISTINCT(ip) as ip,name as name,country as country from ip_details WHERE date(ts) >= date(?) and date(ts) <= date(?)) AS B On a.sip = b.ip ORDER BY ts DESC LIMIT " . $opts->{limit} . ";";
    }

    $sth = $dbh->prepare($query);
    if(!defined $sth){
      return(0, "SSHMonitor: Cannot prepare statement: " . $dbh->errstr); 
    }
    $sth->execute($begin, $end, $begin, $end) or syslog("info", "SSHMonitor: Cannot execute statement: " . $dbh->errstr);
    my @attacks = ();
    # save table array of hashes
    while(my @row = $sth->fetchrow_array()){
      foreach(@row){
        push(@attacks, $_);
      }
    }
    $sth->finish();
    $args{'attacks'} = \@attacks;
  }
  my $query;
  #get Data for Scans
  if($graph_name eq 'highchart_scans'){
    if(!$SQLITE){
      $query = "SELECT ts,sip,name,country,dip,flows FROM (SELECT * FROM scans WHERE ts >= ? AND ts <= ?";
    } else {
      $query = "SELECT ts,sip,name,country,dip,flows FROM (SELECT * FROM scans WHERE datetime(ts) >= datetime(?) AND datetime(ts) <= datetime(?)";
    }

    if($opts->{src_ip} =~ m/^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$/x){
      $query .= " and sip = '".$opts->{src_ip}."'";
    }
    if($opts->{dst_ip} =~ m/\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}/x){
      $query .= " and dip = '".$opts->{dst_ip}."'";
    }
    if(!$SQLITE){
      $query .= ") AS A INNER JOIN (SELECT DISTINCT(ip),name,country FROM ip_details WHERE date(ts) >= ? and ts <= ?) AS B On a.sip = b.ip ORDER BY ts DESC LIMIT " . $opts->{limit} . ";";
    }else{
      $query .= ") AS A INNER JOIN (SELECT DISTINCT(ip) as ip,name as name,country as country FROM ip_details WHERE date(ts) >= date(?) and date(ts) <= date(?)) AS B On a.sip = b.ip ORDER BY ts DESC LIMIT " . $opts->{limit} . ";";
    }
    $sth = $dbh->prepare($query); 
    if(!defined $sth){
      return(0, "SSHMonitor: Cannot prepare statement: " . $dbh->errstr); 
    }
    $sth->execute($begin,$end,$begin, $end) or syslog("info", "SSHMonitor: Cannot execute statement: " . $dbh->errstr);
    my @scans = ();
    # save table array of hashes
    while(my @row = $sth->fetchrow_array()){
      foreach(@row){
        push(@scans, $_);
      }
    }
    $sth->finish();
    $args{'scans'} = \@scans;
  }

  $dbh->disconnect or warn("SSHMonitor: Disconnection from DB failed " . $dbh->errstr);

  if($PROFILING){
    my $GetDataFromPgTotal = sprintf("%.6f", Time::HiRes::time() -  $GetDataFromPgTime);
    syslog("info", "RdpMonito: PROFILING: GetDataFromPg: $graph_name - $GetDataFromPgTotal sec.");
  }
  Nfcomm::socket_send_ok ($socket, \%args);
  return(1);
}


=item
 FeedGraph can be called from frontend as feed_graph function. Returns data
 for Highchart graphs.

 Which data will be sent to fronted depends on the value of "graph_name"
 option in $opts array.

 Input: $socket - nfsen socket, handle of communication socket to the frontend
        $opts - hashref, options sent from frontend
 Output: 
   On success: send data over nfsen socket
   On error: (0, 'error message')
=cut
sub FeedGraph 
{
  my $feedGraphTime = Time::HiRes::time() if ($PROFILING);

  my $socket      = shift;
  my $opts        = shift;
  my $graph_name  = $opts->{'graph_name'};
  my $renderTo    = $opts->{'render_to'};

  # Default values
  my @line1 = (1,1);
  my @line2 = (1,1);
  my @line3 = (1,1);
  my @ipDetails = (1,1);
  my @ipDetailsMonthly = (1,1);
  my @top10_victims = (1,1);
  my @top10_attackers = (1,1);

  my $rc;
  my $result;

  if($SQLITE){
    ($rc, $result) = ConnectToSQLite();
  } else {
    ($rc, $result) = ConnectToPg();
  }

  if(!$rc){
    return(0, "DB connection failed: $result");
  }
  #DB handler
  my $dbh = $result;

  my @data = ();
  my $sth;

  # gather all data into %args hash and send it to the frontend
  my %args;
  $args{"render_to"} = $renderTo;
  my ($begin, $end);

  my $current_time = DateTime->now(time_zone => 'local');
  if($graph_name eq 'highchart_attacks' or $graph_name eq 'highchart_scans'){
    $begin = DateTime->from_epoch(epoch => $opts->{'begin'});
    $begin->set_time_zone('local');#$TIMEZONE
    $end = DateTime->from_epoch(epoch => $opts->{'end'});
    $end->set_time_zone('local');#$TIMEZONE);
    if(!$SQLITE){
      $sth = $dbh->prepare("SELECT * FROM stats_unique WHERE ts >= ? and ts <= ? ORDER BY ts");
    }else{
      $sth = $dbh->prepare("SELECT datetime(ts) as ts,distinct_attackers,distinct_attack_victims,distinct_scanners,distinct_scan_victims,scan_count FROM stats_unique WHERE datetime(ts) >= datetime(?) and datetime(ts) <= datetime(?) ORDER BY ts");
    }

    $sth->execute($begin,$end) or syslog("info", "SSHMonitor: Query execution failed: " . $dbh->errstr);
    while(my $row = $sth->fetchrow_hashref()){
      push(@data, $row);
    }
  }

  #create data sets for attacks higcharts
  if($graph_name eq "highchart_attacks"){
    @line1 = ();
    @line2 = ();
    while(($data[0]->{'ts'} ne $begin->strftime('%Y-%m-%d %H:%M:%S')) and @data){
      push(@line1, 'null');
      push(@line2, 'null');
      $begin->add(minutes => 5);
      last if (DateTime->compare($begin,$current_time) == 1);
    }
    foreach my $entry (@data){
      until($entry->{'ts'} eq $begin->strftime('%Y-%m-%d %H:%M:%S')){
        push(@line1, 'null');
        push(@line2, 'null');
        $begin->add(minutes => 5);
        last if (DateTime->compare($begin,$current_time) == 1);
      }
      push(@line1, $entry->{'distinct_attackers'});
      push(@line2, $entry->{'distinct_attack_victims'});
      $begin->add(minutes => 5);
    }
    $args{"line1"} = \@line1;
    $args{"line2"} = \@line2;
  }
  #create data sets for scans highcharts
  if($graph_name eq "highchart_scans"){
    @line1 = ();
    @line2 = ();
    @line3 = ();
    while(($data[0]->{'ts'} ne $begin->strftime('%Y-%m-%d %H:%M:%S')) and @data){
      push(@line1, 'null');
      push(@line2, 'null');
      push(@line3, 'null');
      $begin->add(minutes => 5);
      last if (DateTime->compare($begin,$current_time) == 1);
    }
    foreach my $entry (@data){
      until($entry->{'ts'} eq $begin->strftime('%Y-%m-%d %H:%M:%S')){
        push(@line1, 'null');
        push(@line2, 'null');
        push(@line3, 'null');
        $begin->add(minutes => 5);
        last if (DateTime->compare($begin,$current_time) == 1);
      }
      push(@line1, $entry->{'distinct_scanners'});
      push(@line2, $entry->{'distinct_scan_victims'});
      push(@line3, $entry->{'scan_count'});
      $begin->add(minutes => 5);
    }
    $args{"line1"} = \@line1;
    $args{"line2"} = \@line2;
    $args{"line3"} = \@line3;
  }

  #returns top countries order by occurence
  if(($graph_name eq "highchart_ip_details") and (!$SQLITE)){
    #return top 7 countries for each month the script is running (sum over months)
    $sth = $dbh->prepare("select to_char,country,count from (SELECT row_number() over(partition by month order by month,count desc) as row_number,to_char(month, 'YYYY Month'),country,count from (SELECT month,country,count FROM (SELECT date_trunc('month', ts) AS month,country,count(distinct(ip)) FROM ip_details WHERE date_trunc('month',ts) >= date_trunc('month', current_date- interval '5 months') GROUP BY month,country ORDER BY month,count DESC) AS a) as B) as C where row_number <=7;");
    $sth->execute() or syslog("info", "SSHMonitor: Query execution failed: " . $dbh->errstr);
    @ipDetailsMonthly = ();
    while(my @row = $sth->fetchrow_array()){
      push(@ipDetailsMonthly, @row);
    } 
    $args{"ipDetailsMonthly"} = \@ipDetailsMonthly;
  }
 
  #return top 10 victims (the most attacked computers in network) for last 7 days 
  if($graph_name eq "highchart_top10_victims"){
    if(!$SQLITE){
      $sth = $dbh->prepare("SELECT dip,count(distinct(sip)) AS count FROM attacks WHERE date(ts) >= current_date - 7 GROUP BY dip ORDER BY count desc LIMIT 10;");
    }else{
      $sth = $dbh->prepare("SELECT dip,count(distinct(sip)) AS count FROM attacks WHERE date(ts) >= datetime('now','-7 days') GROUP BY dip ORDER BY count desc LIMIT 10;");
    }
    $sth->execute() or syslog("info", "SSHMonitor: Query execution failed: " . $dbh->errstr);
    @top10_victims = ();
    while(my @row = $sth->fetchrow_array()){
      push(@top10_victims, @row); 
    } 
    $args{"top10_victims"} = \@top10_victims;
  }
  
  #return top 10 attackers (attackers which attacked the most computers) for last 7 days
  if($graph_name eq "highchart_top10_attackers"){
    if(!$SQLITE){
      $sth = $dbh->prepare("SELECT sip,count,name,country FROM (SELECT sip,count(distinct(dip)) AS count FROM attacks WHERE date(ts)>=current_date-7 GROUP BY sip ORDER BY count DESC LIMIT 10) AS A INNER JOIN (SELECT distinct(ip),name,country FROM ip_details WHERE date(ts)>=current_date-7) AS B ON a.sip = b.ip ORDER BY count DESC;");
    }else{
      $sth = $dbh->prepare("SELECT sip,count,name,country FROM (SELECT sip as sip,count(distinct(dip)) as count FROM attacks WHERE date(ts)>=datetime('now','-7 days') GROUP BY sip ORDER BY count DESC LIMIT 10) AS A INNER JOIN (SELECT distinct(ip) as ip,name as name,country as country FROM ip_details WHERE date(ts)>=datetime('now','-7 days')) AS B ON a.sip = b.ip ORDER BY count DESC;");
    }
    $sth->execute() or syslog("info", "SSHMonitor: Query execution failed: " . $dbh->errstr);
    @top10_attackers = ();
    while(my @row = $sth->fetchrow_array()){
      push(@top10_attackers, @row); 
    } 
    $args{"top10_attackers"} = \@top10_attackers;
  }

  
  $dbh->disconnect or warn("SSHMonitor: Disconnection from DB failed " . $dbh->errstr);
  
  if($PROFILING){
    my $feedGraphTotal = sprintf("%.6f", Time::HiRes::time() -  $feedGraphTime);
    syslog("info", "SSHMonitor: PROFILING: FeedGraph: $graph_name - $feedGraphTotal sec.");
  }

  Nfcomm::socket_send_ok ($socket, \%args);
  return(1);
}

=item
 The Init function is called when the plugin is loaded. Its purpose is to give the plugin 
 the possibility to initialize itself. The plugin should return 1 for success or 0 for 
 failure. If the plugin fails to initialize, it's disabled and not used. Therefore, if
 you want to temporarily disable your plugin return 0 when Init is called.

 Output:
   On success: (1, 'SSHMonitor: Script initialized')
   On error: (0, 'error message')
=cut
sub Init 
{
  syslog("info", "SSHMonitor: plugin init");
  
  # Init variables from config
  $NFDUMP  = "$NfConf::PREFIX/nfdump";
  $PROFILEDIR = "$NfConf::PROFILEDATADIR";

  # get plugin configuration from nfsen.conf
  my $conf = $NfConf::PluginConf{SSHMonitor};

  # mail for reports
  $MAIL_TO = $conf->{'mail_to'};
  # nfdump configuration - data channels in/out, scans
  $CHANNEL_IN    = $conf->{'channel_in'};
  $CHANNEL_OUT   = $conf->{'channel_out'};
  $CHANNEL_SCAN  = $conf->{'channel_scan'};
  $LOCAL_NET     = $conf->{'local_network'};
  # PostgreSQL db connection configuration
  $SQLITE        = $conf->{'sqlite'};
  if($SQLITE){
    $SQLITE_PATH    = $conf->{'sqlite_path'};
  } else {
    $DB_HOST       = $conf->{'db_host'};
    $DB_PORT       = $conf->{'db_port'};
    $DB_NAME       = $conf->{'db_name'};
    $DB_USER       = $conf->{'db_user'};
    $DB_PASSWD     = $conf->{'db_passwd'};
  }
  # interval - time to search for attacks happened before

  my $whitelist_conf   = $conf->{'whitelist'};


  $INTERVAL      = '7 days';
  #copy configuration to frontend
  $NFCONF = $conf;
 
  #parse whitelist from nfsen.conf 
  $whitelist_conf   =~ s/ //g;
  @WHITELIST   = split(/,/, $whitelist_conf);

  $TIMEZONE = DateTime->now(time_zone => 'local');
  my $formatter = DateTime::Format::Strptime->new(pattern => "%z");
  $TIMEZONE->set_formatter($formatter);

  #always check if DB is ready
  if(!$SQLITE){
    my ($rc, $result) = CheckPgTable();
    if(!$rc){
      syslog("info", "SSHMonitor: $CHANNEL_IN $INTERVAL Initialization failed: $result");
      return(0, "SSHMonitor: Initialization failed: $result");
    }
  }

  syslog("info", "SSHMonitor plugin: Initialized: mail_to = $MAIL_TO, channel_in = $CHANNEL_IN, channel_out = $CHANNEL_OUT, channel_scan = $CHANNEL_SCAN whitelist = ".$conf->{'whitelist'});

  return(1, "SSHMonitor: Script initialized");
}

=item
 The Cleanup function is called, when nfsend terminates. Its purpose is to give the
 plugin the possibility to cleanup itself. Its return value is discarded.
=cut
sub Cleanup {
  syslog("info", "SSHMonitor plugin Cleanup");
  return 1;
}

=item
  trim - remove spaces from in front of and behind the string
  
  Input: 'string'
  Ouptu: 'trimmed string'
=cut
# trim spaces from string
sub trim
{
  my $string = $_[0];
  $string =~ s/^\s+//x;
  $string =~ s/\s+$//x;
  return($string);
}

=item
 getCymruOrigin - obtain origin (AS number) from whois.cymru.com
 
 Input: IP address
 Output: 
   On success: (1, 'origin')
   On error:   (0, 'error message')
=cut
sub getCymruOrigin
{
  my $ip = shift;

  my $cymru = `whois -h whois.cymru.com "$ip"`;
  
  my @cymru_array = split("\n", $cymru);
  my @cymru_item = split(/\|/x, $cymru_array[3]);
  my $origin = trim($cymru_item[0]);

  if ($origin =~ m/NA/x){
    $origin = "null";
  }

  return (1, $origin);

} # End of getCymruOrigin

=item
 reportToWarden - report incident to Warden
 
 Input: 
   source of attack - (IP address)
   time detected - (timestamp)    
   attacks_scale - scale of attack (integer)
 Output: 
   returns 1;
=cut
sub reportToWarden {
        # Path to warden-client folder
        my $warden_path = '/opt/warden-client';

        # Inclusion of warden-client sender module
        require $warden_path . '/lib/WardenClientSend.pm';

        my $source = shift;
        my $detected = shift;
        my $attack_scale = shift;

        my $service             = "SSHBruteForce-1_N";
        my $type                = "bruteforce";
        my $source_type         = "IP";
        my $target_proto        = "TCP";
        my $target_port         = "22";
        my $note                = "attack scale = # of attacked hosts";
        my $priority            = undef;
        my $timeout             = 60*24; # 1 day

        my @event = ($service, $detected, $type, $source_type, $source, $target_proto, $target_port, $attack_scale, $note, $priority, $timeout);

        # Sending event to Warden server - undefined return value
        WardenClientSend::saveNewEvent($warden_path, \@event);

        return 1;
}

#
# Periodic data processing function
#	input:	hash reference including the items:
#			'profile'		profile name
#			'profilegroup'	profile group
#			'timeslot' 		time of slot to process: Format yyyymmddHHMM e.g. 200503031200
sub run
{
  # time for profiling purposes
  syslog("info", "SSHMonitor started");
  my $total_time = Time::HiRes::time();

  my $argref       = shift;

  my $profile      = $$argref{'profile'};
  my $profilegroup = $$argref{'profilegroup'};
  my $timeslot     = $$argref{'timeslot'};

  syslog('info', "SSHMonitor plugin run: Profilegroup: $profilegroup, Profile: $profile, Time: $timeslot");

  my %profileinfo     = NfProfile::ReadProfile($profile, $profilegroup);
  my $profilepath     = NfProfile::ProfilePath($profile, $profilegroup);
  my $source          = "$PROFILEDIR/$profilepath";

  # Convert given timeslot into unix timestamp format
  $timeslot =~ m/(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})/x;
  my $year = $1;
  my $month = $2;
  my $day = $3;

  my $input_file    = "$year/$month/$day/nfcapd.$timeslot";

  my $rc;
  my $result;

  if($SQLITE){
    ($rc, $result) = ConnectToSQLite();
  } else {
    ($rc, $result) = ConnectToPg();
  }

  if(!$rc){
    syslog("info", "SSHMonitor: Run failed: $result");
    return(0, "SSHMonitor: Run failed: $result");
  }
  my $dbh = $result;

  $dbh->{AutoCommit} = 1;
  open EMAIL_H, "<", "$NfConf::BACKEND_PLUGINDIR/SSHMonitor/email.txt";
  my $email_text = '';
  while(<EMAIL_H>){
    next if($_ =~ m/^#/);
    $email_text .= $_;
  }
  close(EMAIL_H);

  my $total_time_scanners = Time::HiRes::time();

  ## find all scanners
  my $nf_filter_scan = 'flags S and not flags ARFUP';

  my @ips = `$NFDUMP -M $source/$CHANNEL_SCAN -T -r $input_file '$nf_filter_scan' -o 'fmt:%ts;%sa;%da;%fl' -a -q`;#-A srcip -q`;
  
   
  my $i = 0;
  my %distinctScanVictims;
  my %distinctScanners;

  my $sthScans = $dbh->prepare("INSERT INTO scans VALUES (?,?,?,?)");
  my @scans_to_file;

  for ($i = 0; $i < @ips; $i++){
    my @flow = split(/;/x,$ips[$i]);
    my $ts = $flow[0];
    my $sip = trim(substr($flow[1],1));
    my $dip = trim(substr($flow[2],1));
    $distinctScanVictims{$dip} = $dip;
    $distinctScanners{$sip} = $sip;
    my $flows = trim(substr($flow[3],1));
    if(!$SQLITE){
      $sthScans->execute($ts,$sip,$dip,$flows) or warn("info", "SSHMonitor: run: Cannot execute query " . $dbh->errstr);
    } else {
      push(@scans_to_file, "$ts|$sip|$dip|$flows");
    }
  }

  if($SQLITE){
    open FILE, ">", "/tmp/ssh_scans_to_db";
    foreach my $line (@scans_to_file){
      print FILE $line."\n";
    }
    close(FILE);
    system qq(sqlite3 /data/nfsen/plugins/SSHMonitor/sshmonitoring ".import /tmp/ssh_scans_to_db scans");
  } else {
    $sthScans->finish();
  }
  #get unique scanners count
  my $scannersCount = scalar keys %distinctScanners;
  syslog('info', "SSHMonitor plugin: $scannersCount scanning (scans $i) IPs inserted!");
  #number of scans
  my $scanCount = $i;
  syslog('info', "SSHMonitor plugin: $scanCount scanns inserted - from $scannersCount attackers!");

  $total_time_scanners = sprintf("%.6f", Time::HiRes::time() - $total_time_scanners);
  syslog('info', "SSHMonitor run: SCANNERS PROCESSING TIME $total_time_scanners sec.");

  # performance analysis - start
  my $attacks_time = 0;
  my $attacks_start_init = sprintf ("%.3f", Time::HiRes::time);

  # attacks
  my $nfdump_ips = "$NFDUMP -M $source/$CHANNEL_IN:$CHANNEL_OUT -T -r $input_file 'bytes > 999 and bytes < 5001 and packets > 9 and packets < 31 and dst port 22 and flags SA' -o 'fmt:%ts;%sa' -a -A srcip -q";
  @ips = `$nfdump_ips`;

  my $attacks_end_init = sprintf ("%.3f", Time::HiRes::time - $attacks_start_init);
  $attacks_time += $attacks_end_init;

  my $reg = IP::Country::Fast->new();
  my $ts_first; # timestamp of the first attack in the current timeslot
  my @blocked;

  if($TEST){
    if(!$SQLITE){
      $dbh->do("DELETE FROM blocked WHERE ts <= now()- interval '4 days';") or syslog("info", "SSHMonitor: query failed: DELETE FROM blocked WHERE ts <= now()- interval '4 days';");
     } else {
      $dbh->do("DELETE FROM blocked WHERE ts <= (datetime('now','-4 days'));") or syslog("info", "SSHMonitor: query failed: DELETE FROM blocked WHERE ts <= now()- interval '4 days';");
     }
     my $sth = $dbh->prepare("SELECT ip FROM blocked;");
     $sth->execute() or syslog("info", "SSHMonitor: Query execution failed: SELECT ip FROM blocked;");
     while(my $ref = $sth->fetchrow_hashref()){
       push(@blocked, $ref->{'ip'}."/32");
     }
  }

  my $victim_flows = 0;
  my %attackers;
  my %victims;

  for ($i = 0; $i < @ips; $i++) {
    my @flow = split(/;/x, $ips[$i]);
    my $ts = $flow[0];
    $ts_first = $ts;

    my $ip = trim(substr($flow[1],1));
 
    if (!defined $attackers{$ip}){
      $attackers{$ip} = $ts_first;
    }

    my $name = scalar gethostbyaddr(inet_aton($ip), AF_INET);
    my $asn = getCymruOrigin($ip);
    my $country = $reg->inet_atocc($ip);

    my $insert = "INSERT INTO ip_details VALUES ('$ts','$ip','$name','$asn','$country');";
    my $sth  = $dbh->prepare($insert);
    my $rv = $sth->execute;
    $sth->finish();
  

    # performance analysis - start (loop)
    my $attacks_start_loop = sprintf ("%.3f", Time::HiRes::time);

    my @victims = `$NFDUMP -M $source/$CHANNEL_IN:$CHANNEL_OUT -T -r $input_file 'src ip $ip and bytes > 999 and bytes < 5001 and packets > 9 and packets < 31 and dst port 22 and flags SA' -o 'fmt:%da;%fl;%ts' -a -A dstip -q`;

    foreach (@victims) {
      my @fields = split(/;/x, $_);
      my $dip = trim(substr $fields[0], 1);
      my $flows = trim($fields[1]);
      my $ts_victim = trim($fields[2]);
      $victims{$dip} = $dip;
      my $insert = "INSERT INTO attacks VALUES ('$ts_victim','$ip','$dip',$flows);";
      $victim_flows += $flows;
      my $sth = $dbh->prepare($insert);
      my $rv = $sth->execute;
      $sth->finish();
    }

    # performance analysis - end
    if ($i == (@ips - 1)) {
      my $attackers_count = $i+1;
      my $flows_ssh = `$NFDUMP -M $source/$CHANNEL_IN:$CHANNEL_OUT -r $input_file -I | grep 'Flows:' | sed 's/Flows: \\([0-9]*\\)/\\1/'`;
      my $flows_mu = `$NFDUMP -r /data/nfsen/profiles-data/live/mu/$input_file -I | grep 'Flows:' | sed 's/Flows: \\([0-9]*\\)/\\1/'`;
      syslog('info', "SSHMonitor run: attackers;time;flows_ssh;flows_mu;victim_flows:$attackers_count;$attacks_time;$flows_ssh;$flows_mu;$victim_flows");
    }

    # correlation: any scanning attempts within last week?
    my $query;
    if(!$SQLITE){
      $query = "SELECT ts FROM scans WHERE sip = '$ip' AND ts BETWEEN TIMESTAMP '$ts_first' - INTERVAL '$INTERVAL' AND '$ts_first' ORDER BY ts DESC LIMIT 1;";
    }else{
    $query = "SELECT ts FROM scans WHERE sip = '$ip' AND ts >= datetime('$ts_first','-$INTERVAL') AND ts <= datetime('$ts_first') ORDER BY ts DESC LIMIT 1;";
    }

    $sth = $dbh->prepare($query);
    $sth->execute();
    my $ref4 = $sth->fetchrow_hashref();
    $sth->finish();
    my $ts_scan = $ref4->{'ts'};
    my $victim_count = scalar @victims;

    #get scan count
    my $query_scan_flows;
    if(!$SQLITE){
      $query_scan_flows = "SELECT sum(flows) AS scan_flows FROM scans WHERE sip = '$ip' AND ts BETWEEN TIMESTAMP '$ts_first' - INTERVAL '$INTERVAL' AND '$ts_first';";
    }else{
      $query_scan_flows = "SELECT sum(flows) AS scan_flows FROM scans WHERE sip = '$ip' AND ts >= datetime('$ts_first','-$INTERVAL') AND ts <= datetime('$ts_first');";
    }

    $sth = $dbh->prepare($query_scan_flows);
    $sth->execute();
    my $ref5 = $sth->fetchrow_hashref();
    $sth->finish();
    my $scan_flows = $ref5->{'scan_flows'};
 
    if ($ts_scan && $victim_count > 5 && $scan_flows >= 10) {
      my $query;
      if(!$SQLITE){
        $query = "SELECT sum(flows) AS attempts FROM attacks WHERE sip = '$ip' AND ts BETWEEN TIMESTAMP '$ts_first' - INTERVAL '$INTERVAL' AND '$ts_first'";
      }else{
        $query = "SELECT sum(flows) AS attempts FROM attacks WHERE sip = '$ip' AND ts >= datetime('$ts_first','-$INTERVAL') AND ts <= datetime('$ts_first')";
      }

      # syslog('info', "SSHMonitor plugin: $query");
      my $sth = $dbh->prepare($query);
      $sth->execute();
      my $ref = $sth->fetchrow_hashref();
      $sth->finish();
      my $attempts = $ref->{'attempts'};

      my $ratio = $attempts/$victim_count;
      my $log = "SSHMonitor plugin: scanner $ip ($name) from $asn ($country) is back and attacking $victim_count hosts by $attempts flows ($ratio flows per host)! last seen scanning $ts_scan ($scan_flows flows), first attack started $ts_first";
      syslog('info', $log);

      # e-mail report iff host is not on whitelist and each victim is attacked by more than 3 flows (in average)
      if (!Net::CIDR::cidrlookup($ip, @WHITELIST) && $ratio > 3 && !Net::CIDR::cidrlookup($ip, @blocked)) {
        my $name_mail = '';
        $name_mail = " ($name)" if ($name ne ''); # format domain name for e-mail

#       my $text = $email_text;
        my $text = "timestamp;victim ip;number of flows\n";
#        $text =~ s/\$sip/$ip/g;
#        $text =~ s/\$name_mail/$name_mail/g;
#        $text =~ s/\$victim_count/$victim_count/g;
#        $text =~ s/\$ts_first/$ts_first/g;
#        $text =~ s/\$attempts/$attempts/g;
#        $text =~ s/\$tz/$TIMEZONE/g;
#        $text =~ s/\$hosts_attacked//g;
 
        my $sth;
        if(!$SQLITE){
          $sth = $dbh -> prepare("SELECT ts, dip, flows FROM attacks WHERE sip='$ip' and ts >= timestamp '$ts_first' - INTERVAL '$INTERVAL' ORDER BY ts ASC limit 20");
        }else{
          $sth = $dbh -> prepare("SELECT ts, dip, flows FROM attacks WHERE sip='$ip' and ts >= datetime('$ts_first','-$INTERVAL') ORDER BY ts ASC limit 20");
        }
        $sth->execute() or syslog('info', "SSHMonitor plugin: Query execution failed: " . $dbh->errstr) and next;

        while ((my $ts, my $dip, my $flows) = $sth->fetchrow_array()){
          $text .= "$ts,$dip,$flows\n";
        }
        $sth->finish();
        #insert IP to blocked to avoid report duplicity in request tracker - after first report script gets next
        #timeslot of flow where IP is still active.
        if($TEST){
          if(!$SQLITE){
            $dbh->do("INSERT INTO blocked (ts,ip) values(now(), '$ip');") or syslog("info", "SSHMonitor: Query failed:". $dbh->errstr);
            $dbh->do("INSERT INTO incidents (tsreported,tsfirst,sip,victimcount,attempts) values(now(),$ts_first,$ip,$victim_count,$attempts);") or syslog("info", "SSHMonitor: Query failed:" . $dbh->errstr);
          } else {
            $dbh->do("INSERT INTO blocked (ts,ip) values(datetime('now','+1 hour'), '$ip')") or syslog("info", "SSHMonitor: Query failed:". $dbh->errstr);
            $dbh->do("INSERT INTO incidents (tsreported,tsfirst,sip,victimcount,attempts) values(datetime('now','+1 hour'),'$ts_first','$ip',$victim_count,$attempts)") or syslog("info", "SSHMonitor: Query failed:" . $dbh->errstr);
          }
        } else {
          my $message = Email::Simple->create
          (
            header => [
             'To'                                  => $MAIL_TO,
             'From'                                => $MAIL_FROM,
             'Subject'                             => 'SSH brute force attacks from '. $ip.$name_mail,
             'X-RT-Tool-Name'                      => 'BruteForceDetector',
             'X-RT-Attack-Type'                    => 'BruteForceAuth',
             'X-RT-Incident-Actor'                 => "IP=$ip",
             'X-RT-Incident-Time'                  => $ts_first,
             'X-RT-BruteForceDetector-VictimCount' => $victim_count,
             'X-RT-BruteForceDetector-Attempts'    => $attempts
            ], 
            body    => $text,
          );

          if (open(my $sendMail, '|/usr/sbin/sendmail -oi -t')){
            print $sendMail $message->as_string;
            close $sendMail;
            syslog('info', 'SSHMonitor: e-mail send to ' . $MAIL_TO);

            #insert IP to blocked to avoid report duplicity in request tracker - after first report script gets next
            #timeslot of flow where IP is still active.
            if(!$SQLITE){
              $dbh->do("INSERT INTO blocked (ts,ip) values(now(), '$ip');") or syslog("info", "SSHMonitor: Query failed:". $dbh->errstr);
            } else {
              $dbh->do("INSERT INTO blocked (ts,ip) values(datetime('now','+1 hour'), '$ip')") or syslog("info", "SSHMonitor: Query failed:". $dbh->errstr);
            }
          } else {
            syslog('info', 'SSHMonitor: error sending e-mail to ' . $MAIL_TO);
          }
  
          # report to Warden server
          my $warden = 0;
          if($warden){
            my $time = DateTime->now();
            $time->set_time_zone("local");
            my $detected = $time->strftime('%FT%T');
            if (reportToWarden($ip, $detected, $victim_count)) {
              syslog('info', "SSHMonitor plugin: reported to Warden server as a new event");
            } else {
              syslog('info', "SSHMonitor plugin: error in reporting to Warden server");
            }
          }
        }
      }
    }
  }


  #prepare stats

  my $ts_db = $timeslot;
  $ts_db =~ s/(....)(..)(..)(..)(..)/$1-$2-$3 $4:$5/gx;

  #distinct attackers
  my $distinctAttackers = scalar keys %attackers;
  #distinct victims
  my $distinctAttackVictims = scalar keys %victims;
  my $distinctScanVictims = scalar keys %distinctScanVictims;

  my $sthStats = $dbh->prepare("INSERT INTO stats_unique (ts,distinct_attackers,distinct_attack_victims,distinct_scanners,distinct_scan_victims,scan_count) VALUES(?,?,?,?,?,?);");
  $sthStats->execute($ts_db,$distinctAttackers,$distinctAttackVictims,$scannersCount,$distinctScanVictims,$scanCount) or syslog("info", "Query execution failed: " .$dbh->errstr);
  $sthStats->finish();

  $dbh->disconnect or warn("SSHMonitor: Disconnection from DB failed " . $dbh->errstr);

  syslog('info', "SSHMonitor plugin: $i attackers inserted!");

  $total_time = sprintf ("%.6f", Time::HiRes::time - $total_time);
  syslog ('info', "SSHMonitor run: TOTAL RUNTIME $total_time sec.");

  return 1;
}

1;
