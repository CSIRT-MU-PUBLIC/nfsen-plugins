# e-mail notification of brute force attack from outer network
#
# lines started with # are comments
# Following variables are exported
# $sip - ip address of attacker
# $name_mail - DNS name of attacker (if known)
# $ts_first - timestamp without time zone of first attack
# $attempts - number of login attempts
# $victim_count - number of victims attacked by given attacker
# $tz - time zone
# $hosts_attacked - detail of attacker flow in our network

# text of email
Greetings,

we would like to notify you that we blocked all incomming
traffic from IP address

   $sip$name_mail

because it has attacked SSH service of $victim_count hosts
in the <name of your newtork> (<your IP range>).

The attacks started at $ts_first$tz and involved $attempts
attempts via SSH service listening on TCP port 22.

List of attacked hosts with timestamps in GMT$tz follows.

Regards,

<your signature>

List of hosts attacked from $sip:

Timestamp in GMT$tz;Destination IP address;Number of attempts

$hosts_attacked
