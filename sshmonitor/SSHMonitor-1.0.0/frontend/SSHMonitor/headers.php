<?php 
 
#
# headers.php -  print headers and javascript functions
#
# Copyright (C) 2012 Masaryk University
# Authors: Jan Vykopal <vykopal@ics.muni.cz>
#          Martin Vizvary, <vizvary@ics.muni.cz>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in
#    the documentation and/or other materials provided with the
#    distribution.
# 3. Neither the name of the Masaryk University nor the names of its
#    contributors may be used to endorse or promote products derived from
#     this software without specific prior written permission.
#
# This software is provided ``as is'', and any express or implied
# warranties, including, but not limited to, the implied warranties of
# merchantability and fitness for a particular purpose are disclaimed.
# In no event shall the Masaryk University or contributors be liable for
# any direct, indirect, incidental, special, exemplary, or consequential
# damages (including, but not limited to, procurement of substitute
# goods or services; loss of use, data, or profits; or business
# interruption) however caused and on any theory of liability, whether
# in contract, strict liability, or tort (including negligence or
# otherwise) arising in any way out of the use of this software, even
# if advised of the possibility of such damage.
#
# Version: 0.9.0
#

 
/*
 * SSHMonitor_PrintHeaders is called at the beginning of SSHMonitor_Run function
 * and prints links to JavaScript and CSS files and prints javascript code
 */
function SSHMonitor_PrintHeaders() {
print '
<!-- ########## JS FILES & LIBRARIES ########### -->
<script language="Javascript" src="plugins/SSHMonitor/js/jquery-min.js" type="text/javascript"></script>
<script language="Javascript" src="plugins/SSHMonitor/js/jquery-ui.min.js" type="text/javascript"></script>
<script language="Javascript" src="plugins/SSHMonitor/js/jquery-ui-timepicker-addon.js" type="text/javascript"></script>
<script language="Javascript" src="plugins/SSHMonitor/js/jquery.easytabs.min.js" type="text/javascript"></script>
<script language="Javascript" src="plugins/SSHMonitor/js/highcharts.js" type="text/javascript"></script>
<script language="Javascript" src="plugins/SSHMonitor/js/exporting.js" type="text/javascript"></script>
<script language="Javascript" src="plugins/SSHMonitor/js/jquery.tablesorter.min.js" type="text/javascript"></script>  	
<!--[if lte IE 8]><script language="javascript" type="text/javascript" src="plugins/backend_test/js/excanvas.min.js"></script><![endif]-->
<!-- ########## CSS FILES & LIBRARIES ########### -->
<link href="plugins/SSHMonitor/css/jquery.tablesorter.min.css" rel="stylesheet" type="text/css"></script>
<link href="plugins/SSHMonitor/css/jquery-ui.css" rel="stylesheet" type="text/css">
<link href="plugins/SSHMonitor/css/SSHMonitor.css" rel="stylesheet" type="text/css">
<script language="Javascript">
$(document).ready( function() {

  // call the tablesorter plugin
  $("table").tablesorter({widgets:["zebra"]});

  // Time format convertor for time-input-panel
  function reformate_time(timeString) {
    return timeString.replace(/(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2})/,"$2/$3/$1 $4:$5");
  }

  // Datetime format function for time-input-panel
  function getFormatedDateTime(d) {
    var year = d.getFullYear();
    var month_num = d.getMonth() +1;
    if (month_num < 10) month = "0" + month_num; else month = "" + month_num;
    var day_num = d.getDate();
    if (day_num < 10) day = "0" + day_num; else day = "" + day_num;
    var hours_num = d.getHours();
    if (hours_num < 10) hours = "0" + hours_num; else hours = "" + hours_num;
    var minutes_num = d.getMinutes();
    if (minutes_num < 10) minutes = "0" + minutes_num; else minutes = "" + minutes_num;
    rval = "" + year + "-" + month + "-" + day + " " + hours + ":" + minutes;
    return rval;
  }
     $("#end_datetime_attacks").datetimepicker({
       dateFormat: "yy-mm-dd", timeFormat: "hh:mm", firstDay: 1, stepMinute: 5,
     });
     $("#begin_datetime_attacks").datetimepicker({
       dateFormat: "yy-mm-dd", timeFormat: "hh:mm", firstDay: 1, stepMinute: 5,
     });

     $("#end_datetime_attacks").datetimepicker({
       dateFormat: "yy-mm-dd", timeFormat: "hh:mm", firstDay: 1, stepMinute: 5,
     });
     $("#begin_datetime_attacks").datetimepicker({
       dateFormat: "yy-mm-dd", timeFormat: "hh:mm", firstDay: 1, stepMinute: 5,
     });

  // according to values of Timewindow and End time sets Begin time and en(dis)ables < > Buttons
  function set_begin_attacks() {
    var d = new Date(reformate_time($("#end_datetime_attacks").val()));
    $("#begin_datetime_attacks").attr("disabled", true);
    $("#prev_button_attacks").attr("disabled", false);
    $("#next_button_attacks").attr("disabled", false);
    switch($("#timewindow_attacks").val()){
      case "6 hours":
        d.setTime(d.getTime() -21600000);
        break;
      case "12 hours":
        d.setTime(d.getTime() -43200000);
        break;
      case "1 day":
        d.setTime(d.getTime() -86400000);
        break; 
      case "1 week":
        d.setTime(d.getTime() -604800000);
        break;
      case "1 month":
        d.setTime(d.getTime() -2419200000);
        break;
      case "other":
        $("#begin_datetime_attacks").attr("disabled", false);
        $("#prev_button_attacks").attr("disabled", true);
        $("#next_button_attacks").attr("disabled", true);
        return;
        break;
      default:
        $("#begin_datetime_attacks").val("error");
        return;
    }
    $("#begin_datetime_attacks").val(getFormatedDateTime(d));
  }

  $("#end_datetime_attacks, #timewindow_attacks").bind($.browser.msie ? "propertychange": "change", function() {
    set_begin_attacks();
  });

  $("#prev_button_attacks").click(function(e) {
    var d = new Date(reformate_time($("#end_datetime_attacks").val()));

    switch($("#timewindow_attacks").val()){ 
      case "6 hours":
        d.setTime(d.getTime() -21600000);
        break;
      case "12 hours":
        d.setTime(d.getTime() -43200000);
        break;
      case "1 day":
        d.setTime(d.getTime() -86400000);
        break; 
      case "1 week":
        d.setTime(d.getTime() -604800000);
        break;
      case "1 month":
        d.setTime(d.getTime() -2419200000);
        break;
      case "other":
        return;
        break;
      default:
        $("#begin_datetime_attacks").val("error");
        return;
    }
    $("#end_datetime_attacks").val(getFormatedDateTime(d));
    set_begin_attacks();
  });

  $("#next_button_attacks").click(function(e) {
    var d = new Date(reformate_time($("#end_datetime_attacks").val()));

    switch($("#timewindow_attacks").val()){ 
      case "6 hours":
        d.setTime(d.getTime() +21600000);
        break;
      case "12 hours":
        d.setTime(d.getTime() +43200000);
        break;
      case "1 day":
        d.setTime(d.getTime() +86400000);
        break; 
      case "1 week":
        d.setTime(d.getTime() +604800000);
        break;
      case "1 month":
        d.setTime(d.getTime() +2419200000);
        break;
      case "other":
        return;
        break;
      default:
        $("#begin_datetime_attacks").val("error");
        return;
    }
    $("#end_datetime_attacks").val(getFormatedDateTime(d));
    set_begin_attacks();
  });

  $("#time_form_attacks").submit(function() {
    $("#begin_datetime_attacks").attr("disabled", false);
  });


     $("#end_datetime_scans").datetimepicker({
       dateFormat: "yy-mm-dd", timeFormat: "hh:mm", firstDay: 1, stepMinute: 5,
     });
     $("#begin_datetime_scans").datetimepicker({
       dateFormat: "yy-mm-dd", timeFormat: "hh:mm", firstDay: 1, stepMinute: 5,
     });



     $("#end_datetime_scans").datetimepicker({
       dateFormat: "yy-mm-dd", timeFormat: "hh:mm", firstDay: 1, stepMinute: 5,
     });
     $("#begin_datetime_scans").datetimepicker({
       dateFormat: "yy-mm-dd", timeFormat: "hh:mm", firstDay: 1, stepMinute: 5,
     });

  // according to values of Timewindow and End time sets Begin time and en(dis)ables < > Buttons
  function set_begin_scans() {
    var d = new Date(reformate_time($("#end_datetime_scans").val()));
    $("#begin_datetime_scans").attr("disabled", true);
    $("#prev_button_scans").attr("disabled", false);
    $("#next_button_scans").attr("disabled", false);
    switch($("#timewindow_scans").val()){
      case "6 hours":
        d.setTime(d.getTime() -21600000);
        break;
      case "12 hours":
        d.setTime(d.getTime() -43200000);
        break;
      case "1 day":
        d.setTime(d.getTime() -86400000);
        break; 
      case "1 week":
        d.setTime(d.getTime() -604800000);
        break;
      case "1 month":
        d.setTime(d.getTime() -2419200000);
        break;
      case "other":
        $("#begin_datetime_scans").attr("disabled", false);
        $("#prev_button_scans").attr("disabled", true);
        $("#next_button_scans").attr("disabled", true);
        return;
        break;
      default:
        $("#begin_datetime_scans").val("error");
        return;
    }
    $("#begin_datetime_scans").val(getFormatedDateTime(d));
  }

  $("#end_datetime_scans, #timewindow_scans").bind($.browser.msie ? "propertychange": "change", function() {
    set_begin_scans();
  });

  $("#prev_button_scans").click(function(e) {
    var d = new Date(reformate_time($("#end_datetime_scans").val()));

    switch($("#timewindow_scans").val()){ 
      case "6 hours":
        d.setTime(d.getTime() -21600000);
        break;
      case "12 hours":
        d.setTime(d.getTime() -43200000);
        break;
      case "1 day":
        d.setTime(d.getTime() -86400000);
        break; 
      case "1 week":
        d.setTime(d.getTime() -604800000);
        break;
      case "1 month":
        d.setTime(d.getTime() -2419200000);
        break;
      case "other":
        return;
        break;
      default:
        $("#begin_datetime_scans").val("error");
        return;
    }
    $("#end_datetime_scans").val(getFormatedDateTime(d));
    set_begin_scans();
  });

  $("#next_button_scans").click(function(e) {
    var d = new Date(reformate_time($("#end_datetime_scans").val()));

    switch($("#timewindow_scans").val()){ 
      case "6 hours":
        d.setTime(d.getTime() +21600000);
        break;
      case "12 hours":
        d.setTime(d.getTime() +43200000);
        break;
      case "1 day":
        d.setTime(d.getTime() +86400000);
        break; 
      case "1 week":
        d.setTime(d.getTime() +604800000);
        break;
      case "1 month":
        d.setTime(d.getTime() +2419200000);
        break;
      case "other":
        return;
        break;
      default:
        $("#begin_datetime_scans").val("error");
        return;
    }
    $("#end_datetime_scans").val(getFormatedDateTime(d));
      set_begin_scans();
  });

  $("#time_form_scans").submit(function() {
    $("#begin_datetime_scans").attr("disabled", false);
  });

});

';
    $color = "['#CCFFFF','#CCFF66','#CCCCFF','#CCCC66','#CC99FF','#CC9966','#CC66FF','#FFFF00','#FFCC66','#FF9966','#FF3366','#CC6666','#CC33FF','#CC3366','#CC00FF','#CC0066','#99FFCC','#99FF33','#99CCCC','#99CC33','#9999CC','#999933','#9966CC','#996633','#9933CC','#993399','#993300','#990099','#990000','#66FF99','#66FF00','#66CC99','#66CC00','#669999','#669900','#666699','#666600','#663399','#663300','#660099','#660000','#33FF99','#33FF00','#33CC99','#33CC00','#339999','#339900','#336699','#336600','#333399','#333300','#330099','#330000','#00FF99','#00FF00','#00CC99','#00CC00','#009999','#009900','#006699','#006600','#003399','#003300','#000099','#000000']";
    $color2 = "['#504A4B','#657383','#2554C7','#4C787E','#4E387E','#151B54','#25383C','#463E41','#151B8D','#2B60DE','#38ACEC','#1569C7','#4863A0','#659EC7','#C12267','#800517','#A74AC7',',#6C2DC7','#7D1B7E',',#43C6DB','#3EA99F','#617C58','#348781','#254117','#667C26']";
    $color3 = "['#00037A','#0015E2','#3351FF','#548AFF','#80B3FF','#AFDEFF','#D8F1FF','#70002B','#B80F00','#FF4004','#FF6217','#FF9D5D','#FFC697','#FFE2C6','#114200','#189500','#1BD300','#38FF2B','#82FF81','#B9FFB8','#DEFFDE']";
    print "
    var color1 = $color;
    var color2 = $color2;
    var color3 = $color3;
    	</script>
  	";
  }
?>
