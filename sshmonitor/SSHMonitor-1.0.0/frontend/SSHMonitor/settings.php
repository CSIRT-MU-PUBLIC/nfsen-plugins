<?php 
#
# settings.php -  prints out settings tab content
#
# Copyright (C) 2012 Masaryk University
# Authors: Jan Vykopal <vykopal@ics.muni.cz>
#          Martin Vizvary, <vizvary@ics.muni.cz>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in
#    the documentation and/or other materials provided with the
#    distribution.
# 3. Neither the name of the Masaryk University nor the names of its
#    contributors may be used to endorse or promote products derived from
#     this software without specific prior written permission.
#
# This software is provided ``as is'', and any express or implied
# warranties, including, but not limited to, the implied warranties of
# merchantability and fitness for a particular purpose are disclaimed.
# In no event shall the Masaryk University or contributors be liable for
# any direct, indirect, incidental, special, exemplary, or consequential
# damages (including, but not limited to, procurement of substitute
# goods or services; loss of use, data, or profits; or business
# interruption) however caused and on any theory of liability, whether
# in contract, strict liability, or tort (including negligence or
# otherwise) arising in any way out of the use of this software, even
# if advised of the possibility of such damage.
#
# Version: 0.9.0
#

/*
 * SSHMonitor_PrintSettings function loads saved settings from backend
 * and prints the settings form with these values.
*/
function SSHMonitor_PrintSettings($opts) {

	// Initialization of opts array (it has to exist when calling backend)
	$opts['option'] = "";
	// Load current settings from backend
	$out_list = nfsend_query("SSHMonitor::load_settings", $opts);

	if(!is_array($out_list)){
	  SetMessage('error', "Error calling plugin backend - load_settings");
	  return FALSE;
	}

    print '
    <table class="data_table" cellpadding="0" cellspacing="0" style="margin: 10px">
      <tr style="background-color: #CEDFDA;">
        <td>
          <b>Key</b>
        </td>
        <td>
          <b>Value</b>
        </td>
      </tr>
      <tr>
        <td>mail_to</td>
        <td>'.$out_list[mail_to].'</td>
      </tr>
      <tr>
        <td>mail_from</td>
        <td>'.$out_list[mail_from].'</td>
      </tr>

      <tr>
        <td>channel_in</td>
        <td>'.$out_list[channel_in].'</td>
      </tr>
      <tr>
        <td>channel_out</td>
        <td>'.$out_list[channel_out].'</td>
      </tr>
      <tr>
        <td>channel_scan</td>
        <td>'.$out_list[channel_scan].'</td>
      </tr>
      <tr>
        <td>local_network</td>
	<td>'.$out_list[local_network].'</td>
      </tr>
      <tr>
        <td valign="top">whitelist</td>
	<td>'.str_replace(",", "<br>", $out_list[whitelist]).'</td>
      </tr>
      <tr style="background-color: #CEDFDA;">
        <td colspan=2>
          <b>DB configuration</b>
        </td>
      </tr>';
      if(!$out_list[sqlite]){
        print '
      <tr>
        <td>db_host</td>
        <td>'.$out_list[db_host].'</td>
      </tr>
      <tr>
        <td>db_port</td>
        <td>'.$out_list[db_port].'</td>
      </tr>
      <tr>
        <td>db_name</td>
        <td>'.$out_list[db_name].'</td>
      </tr>
      <tr>
        <td>db_user</td>
        <td>'.$out_list[db_user].'</td>
      </tr>
      <tr>
        <td>db_passwd</td>
        <td style="font-style: italic;">Readable only in nfsen.conf!</td>
      </tr>';
      } else {
        print '
        <tr>
          <td>sqlite_path</td>
          <td>'.$out_list[sqlite_path].'</td>
        </tr>';
      }
     print '</table>';

} // End of  SSHMonitor_PrintSettings

// ======================================================= TAB Settings
  if(isset($_POST['save_email'])){
    $text = $_POST['save_email'];
    $text = urlencode($text);
    $opts['text'] = "$text";
    $out_list = nfsend_query("SSHMonitor::save_settings", $opts);
  }
 
  print '
  <div style="margin: 20px;"> 
    <table>
      <tr>
        <td valign=top>
        <h3>&nbsp;&nbsp;&nbsp;SSHMonitor settings at nfsen.conf</h3> 
&nbsp;&nbsp;&nbsp;Change of variables must be made in nfsen.conf file.
	</td>
	<td>
          <div style="width: 30px;">&nbsp;</div>
	</td>
	<td>
        <h3>&nbsp;&nbsp;&nbsp;Text of e-mail</h3>
	</td>
	<td valign=top align=right>
          <input type="checkbox" value="Read only" title="Check to edit text of e-mail" "id=\'change_readonly_state\' onClick="document.getElementById(\'save_email\').readOnly=!document.getElementById(\'save_email\').readOnly"/><a title="Check to edit text of e-mail">&nbsp;Editable&nbsp;&nbsp;&nbsp;</a>
	</td>
	<td>
          <form id="edit_email" name="edit_email" action="" method="post">
          <input type=submit value="Save email" />
	</td>
	</tr>
	<tr>
	<td colspan=2 valign=top>';
        SSHMonitor_PrintSettings();
	print '
        </td>
        <td colspan=3>
          <textarea id="save_email" name="save_email" rows="35" cols="70" style="margin: 10px" readonly>'.file_get_contents("/data/nfsen/plugins/SSHMonitor/email.txt").'</textarea></p>
        </td>
      </tr>
      </form>
    </table>
  </div>';
?>
