<?php 
  
  /*
   *SSHMonitor_get_highchart_scans function is called from the SSHMonitor_Run function.
   * It takes one argument - array otps with options for backend function. It must
   * contain name of the graph (under graph_name key) and unix timestamps of begin
   * and end of the displayed data (under keys begin and end).
   *
   * The function will request data from backend and returns JavaScript code that
   * will draw the graph with Highcharts library.
   */
  function SSHMonitor_get_highchart_scans($opts) {
  	// Request the data for the graph from backend function feedGraph

          $out_list = nfsend_query("SSHMonitor::feed_graph", $opts);
          if ( !is_array($out_list) ) {
                  print "Error calling backend plugin - feed_graph SSHMonitor_get_highchart_scans<br> $out_list<br>";
                  return FALSE;
          }
          $data_line1      = $out_list["line1"];
          $data_line2      = $out_list["line2"];
          $data_line3      = $out_list["line3"];
  	// Generate JavaScript arrays definitions of graph lines
          $js_code = "line1 = [";
          $js_code .= join(',',$data_line1);
          $js_code .= "];\n";
          $js_code .= "line2 = [";
          $js_code .= join(',',$data_line2);
          $js_code .= "];\n";
          $js_code .= "line3 = [";
          $js_code .= join(',',$data_line3);
          $js_code .= "];\n";
          $js_code .= 'renderTo = "'. $out_list["render_to"].'";';

          $js_code .= '
 
  var current_date = new Date();
  var current_timezone = current_date.getTimezoneOffset();
  
  chart1 = new Highcharts.Chart({
  	chart: {
  		renderTo: renderTo,
                zoomType: "x",
  		type: "line",
                borderWidth: 1,
                plotBorderWidth: 1
  	},
  	title: {
  		text: "Scan trends",
  		style: { color: "black" }
  
  	},';
        if($out_list["render_to"] == 'highchart-scans'){
           $js_code .= 'subtitle: {
	               text: "Shows count of scans in selected window"
	               },';
	} else {
	   $js_code .= 'subtitle: {
	              text: "Shows attacks in 1 day window backwards from now"
		      },';
        }
        $js_code .= '
  	plotOptions: {
                  line: {  marker: { enabled: false,lineWidth: 1,radius: 2 }}
  
  	},
  	tooltip: {
      useHTML: true,
      shadow: false,
  		formatter: function() {
                  if(this.y === 0.00001){
                     var y1 = 0;
                  } else {
                     var y1 = this.y;
                  }
               	return \'\'+
  		Highcharts.dateFormat(\'Timeslot: %e. %b %Y, %H:%M\', this.x) +\' - \'+ y1;
  		}
  	},
  	xAxis: {
  		type: "datetime",
  	},
  	yAxis: {
    	     title: {
  	         text: "Quantity" 
  	     },
             min: 0,
             type: "linear"
  	},
  	series: [
  		{ name: "Unique scanners", data: line1,  pointStart: ('.$opts["begin"].'*1000 - current_timezone * 60 * 1000) ,pointInterval: 5 * 60 * 1000 },
  		{ name: "Unique victims of scan", data: line2,  pointStart: ('.$opts["begin"].'*1000 - current_timezone * 60 * 1000) ,pointInterval: 5 * 60 * 1000 },
      { name: "Scan Count", data: line3,  pointStart: ('.$opts["begin"].'*1000 - current_timezone * 60 * 1000) ,pointInterval: 5 * 60 * 1000 },
  	]
  });
  chart1.series[1].hide();
  chart1.series[2].hide();
  	';
  	return $js_code;
  }
?>
