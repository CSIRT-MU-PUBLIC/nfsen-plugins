CREATE TABLE attacks (
    ts timestamp without time zone,
    sip inet,
    dip inet,
    flows integer
);

CREATE TABLE ip_details (
    ts timestamp without time zone,
    ip inet,
    name character varying(256),
    ans integer,
    country character(2)
);


CREATE TABLE scans (
    ts timestamp without time zone,
    sip inet,
    dip inet,
    flows integer
);
