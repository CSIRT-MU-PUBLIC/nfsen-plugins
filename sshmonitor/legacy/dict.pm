#!/usr/bin/perl
#
# TODO: check license
# dict.pm -  NfSen plugin for massive brute force attack detection 
# 
# Copyright (C) 2010, 2011 Masaryk University
# Author: Jan VYKOPAL <vykopal@ics.muni.cz>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in
#    the documentation and/or other materials provided with the
#    distribution.
# 3. Neither the name of the Masaryk University nor the names of its
#    contributors may be used to endorse or promote products derived from
#     this software without specific prior written permission.
#
# This software is provided ``as is'', and any express or implied
# warranties, including, but not limited to, the implied warranties of
# merchantability and fitness for a particular purpose are disclaimed.
# In no event shall the Masaryk University or contributors be liable for
# any direct, indirect, incidental, special, exemplary, or consequential
# damages (including, but not limited to, procurement of substitute
# goods or services; loss of use, data, or profits; or business
# interruption) however caused and on any theory of liability, whether
# in contract, strict liability, or tort (including negligence or
# otherwise) arising in any way out of the use of this software, even
# if advised of the possibility of such damage.
#
# $Id: dict.pm 2152 2013-08-05 13:15:39Z 207426 $

package dict;

use strict;
use NfProfile;
use NfConf;

use DBD::Pg;
use DBI;

use IP::Country::Fast;
use Socket;
use Sys::Syslog;
use MIME::Lite;
use POSIX;
#use Net::IP::Match::Regexp qw( create_iprange_regexp match_ip );
use Net::CIDR;
use DateTime;
use DateTime::Format::Strptime;
use Time::HiRes;

our %cmd_lookup = (
	'getStats'	=> \&getStats,
);

# This string identifies the plugin as a version 0.1.1 plugin. 
our $VERSION = 011;

my $EODATA 	= ".\n";

my ($nfdump, $PROFILEDIR, $mail_to, $channel_in, $channel_out, $db_host, $db_port, $db_name, $db_user, $db_passwd, @whitelist, $tz);

# Define a nice filter: 
my $nf_filter = 'proto tcp and dst port 22';

sub getStats {
	my $socket  = shift;	        # scalar
	my $opts	= shift;	# reference to a hash

        my $dsn = "dbi:Pg:database=$db_name;host=$db_host;port=$db_port";
        my $dbh = DBI->connect ($dsn, $db_user, $db_passwd) or syslog('debug', "dict plugin: Cannot connect to database: " . DBI->errstr);

	# error checking example
#	if ( !exists $$opts{'colours'} ) {
#		Nfcomm::socket_send_error($socket, "Missing value");
#		return;
#	}
        # my $query = "select count(*) as num from scans where ts between to_timestamp('$timeslot', 'YYYYMMDDHH24MISS') - interval '5 mins' and to_timestamp('$timeslot', 'YYYYMMDDHH24MISS');";
        my $query = "select count(*) as num from scans where ts between now() - interval '10 mins' and now() - interval '5 mins';";
        my $sth = $dbh->prepare($query);
        $sth->execute();
        my $ref = $sth->fetchrow_hashref();
        $sth->finish();

        my $query = "select count(*) as num from scans where ts between now() - interval '1 hour 5 mins' and now() - interval '5 mins';";
        my $sth = $dbh->prepare($query);
        $sth->execute();
        my $ref2 = $sth->fetchrow_hashref();
        $sth->finish();

        my $query = "select count(*) as num from scans where ts between now() - interval '1 day 5 mins' and now() - interval '5 mins';";
        my $sth = $dbh->prepare($query);
        $sth->execute();
        my $ref3 = $sth->fetchrow_hashref();
        $sth->finish();

        my $query = "select distinct ip,count,name,ans,country from (select sip, count(*) from attacks where attacks.ts between now() - interval '10 minutes' and now() - interval '5 mins' group by sip having count(*) > 10) as attackers, ip_details where attackers.sip = ip_details.ip;";
        my $sth = $dbh->prepare($query);
        $sth->execute();
        my @attackers = ();
        while ((my $ip, my $count, my $name, my $asn, my $country) = $sth->fetchrow_array()) {

                my $query = "select ts from scans where sip = '$ip' order by ts desc limit 1;";
                my $sth = $dbh->prepare($query);
                $sth->execute();
                my $ref4 = $sth->fetchrow_hashref();
                $sth->finish();
                my $ts_scan = $ref4->{'ts'};
                push(@attackers, "$ip | $count | $name | $asn | $country | $ts_scan");
        }
        $sth->finish();

        $dbh->disconnect();

#	syslog('info', "dict plugin: $query".$ref);
	# Prepare answer
	my %args;
	$args{'count-5mins'} = $ref->{'num'};
	$args{'count-hour'} = $ref2->{'num'};
	$args{'count-day'} = $ref3->{'num'};
        $args{'attackers'} = \@attackers;

	Nfcomm::socket_send_ok($socket, \%args);

} # End of RunProc

#
# Periodic data processing function
#	input:	hash reference including the items:
#			'profile'		profile name
#			'profilegroup'	profile group
#			'timeslot' 		time of slot to process: Format yyyymmddHHMM e.g. 200503031200
sub run {
	# Log runtime
	my $total_time = Time::HiRes::time;

	my $argref 		 = shift;
	my $profile 	 = $$argref{'profile'};
	my $profilegroup = $$argref{'profilegroup'};
        my $timeslot 	 = $$argref{'timeslot'};

	syslog('debug', "dict plugin run: Profilegroup: $profilegroup, Profile: $profile, Time: $timeslot");

	my %profileinfo     = NfProfile::ReadProfile($profile, $profilegroup);
	my $profilepath 	= NfProfile::ProfilePath($profile, $profilegroup);
        my $source =  "$PROFILEDIR/$profilepath";

        my $dsn = "dbi:Pg:database=$db_name;host=$db_host;port=$db_port";
        my $dbh = DBI->connect ($dsn, $db_user, $db_passwd) or syslog('debug', "dict plugin: Cannot connect to database: " . DBI->errstr);

        my $year = substr($timeslot, 0, 4);
        my $month = substr($timeslot, 4, 2);
        my $day = substr($timeslot, 6, 2);
        my $input_file = "$year/$month/$day/nfcapd.$timeslot";

        # scans
        my $nfdump_ips = "$nfdump -M $source/$channel_in -T -r $input_file 'flags S and not flags ARPUF' -o 'fmt:%ts;%sa;%da;%fl' -a -A srcip,dstip -q";
        my @ips = `$nfdump_ips`;

        my $i;
        # optimalizace -o fmt:insert into...
        for ($i = 0; $i < @ips; $i++) {
                my @flow = split(/;/,$ips[$i]);
                my $ts = $flow[0];
                my $sip = trim(substr $flow[1], 1);
                my $dip = trim(substr $flow[2], 1);
                my $flows = trim($flow[3]);
                my $insert = "INSERT INTO scans VALUES ('$ts','$sip','$dip',$flows);";
                my $sth  = $dbh->prepare($insert);
                my $rv = $sth->execute;
                $sth->finish();
        }

	syslog('info', "dict plugin: $i scanning IPs inserted!");

	# performance analysis - start
	my $attacks_time = 0;
	my $attacks_start_init = sprintf ("%.3f", Time::HiRes::time);

        # attacks
        $nfdump_ips = "$nfdump -M $source/$channel_in:$channel_out -T -r $input_file 'bytes > 999 and bytes < 5001 and packets > 9 and packets < 31 and dst port 22 and flags SA' -o 'fmt:%ts;%sa' -a -A srcip -q";
        @ips = `$nfdump_ips`;

	my $attacks_end_init = sprintf ("%.3f", Time::HiRes::time - $attacks_start_init);
	my $attacks_time += $attacks_end_init;

        my $reg = IP::Country::Fast->new();
        my $ts_first; # timestamp of the first attack in the current timeslot

        $dbh->do("DELETE FROM blocked WHERE ts <= now()- interval '4 days';") or syslog("info", "query failed: DELETE FROM blocked WHERE ts <= now()- interval '4 days';");

        my $sth = $dbh->prepare("SELECT ip FROM blocked;");
        $sth->execute() or syslog("info", "Query execution failed: SELECT ip FROM blocked;");
        my @blocked;
        while(my $ref = $sth->fetchrow_hashref()){
          push(@blocked, $ref->{'ip'}."/32");
        }

	my $victim_flows = 0;
        for ($i = 0; $i < @ips; $i++) {
                my @flow = split(/;/,$ips[$i]);
                my $ts = $flow[0];
                $ts_first = $ts;

                my $ip = trim(substr $flow[1], 1);
                my $name = scalar gethostbyaddr(inet_aton($ip), AF_INET);
                my $asn = getCymruOrigin($ip);
                my $country = $reg->inet_atocc($ip);

                my $insert = "INSERT INTO ip_details VALUES ('$ts','$ip','$name','$asn','$country');";
                my $sth  = $dbh->prepare($insert);
                my $rv = $sth->execute;
                $sth->finish();

		# performance analysis - start (loop)
		my $attacks_start_loop = sprintf ("%.3f", Time::HiRes::time);

                my @victims = `$nfdump -M $source/$channel_in:$channel_out -T -r $input_file 'src ip $ip and bytes > 999 and bytes < 5001 and packets > 9 and packets < 31 and dst port 22 and flags SA' -o 'fmt:%da;%fl;%ts' -a -A dstip -q`;

                foreach (@victims) {
                        my @fields = split(/;/, $_);
                        my $dip = trim(substr $fields[0], 1);
                        my $flows = trim($fields[1]);
                        my $ts_victim = trim($fields[2]);

                        my $insert = "INSERT INTO attacks VALUES ('$ts_victim','$ip','$dip',$flows);";
			$victim_flows += $flows;
                        my $sth = $dbh->prepare($insert);
                        my $rv = $sth->execute;
                        $sth->finish();
                }

		# performance analysis - end (loop)
		$attacks_time += sprintf ("%.3f", Time::HiRes::time - $attacks_start_loop);
		
		# performance analysis - end
		if ($i == (@ips - 1)) {
			my $attackers_count = $i+1;
			my $flows_ssh = `$nfdump -M $source/$channel_in:$channel_out -r $input_file -I | grep 'Flows:' | sed 's/Flows: \\([0-9]*\\)/\\1/'`;
			my $flows_mu = `$nfdump -r /data/nfsen/profiles-data/live/mu/$input_file -I | grep 'Flows:' | sed 's/Flows: \\([0-9]*\\)/\\1/'`;
			syslog('info', "dict run: attackers;time;flows_ssh;flows_mu;victim_flows:$attackers_count;$attacks_time;$flows_ssh;$flows_mu;$victim_flows");
		}

                # correlation: any scanning attempts within last week?
                my $query = "select ts from scans where sip = '$ip' and ts between timestamp '$ts_first' - interval '7 days' and '$ts_first' order by ts desc limit 1;";
                my $sth = $dbh->prepare($query);
                $sth->execute();
                my $ref4 = $sth->fetchrow_hashref();
                $sth->finish();
                my $ts_scan = $ref4->{'ts'};
                my $victim_count = scalar @victims;

                # jak moc masivni skenovani predchazelo prvnimu utoku?
                my $query_scan_flows = "select sum(flows) as scan_flows from scans where sip = '$ip' and ts between timestamp '$ts_first' - interval '7 days' and '$ts_first';";
                my $sth = $dbh->prepare($query_scan_flows);
                $sth->execute();
                my $ref5 = $sth->fetchrow_hashref();
                $sth->finish();
                my $scan_flows = $ref5->{'scan_flows'};
                # TODO: victim_count and scan_flows => nfsen.conf
                # syslog('info', "dict plugin: last scan: $ts_scan, victim_count: $victim_count, scan_flows: $scan_flows");
                if ($ts_scan && $victim_count > 10 && $scan_flows > 10) {
                         my $query = "select sum(flows) as attempts from attacks where sip = '$ip' and ts >= '$ts_first'";
                         # syslog('info', "dict plugin: $query");
                         my $sth = $dbh->prepare($query);
                         $sth->execute();
                         my $ref = $sth->fetchrow_hashref();
                         $sth->finish();
                         my $attempts = $ref->{'attempts'};

                         my $ratio = $attempts/$victim_count;

                         my $log = "dict plugin: scanner $ip ($name) from $asn ($country) is back and attacking $victim_count hosts by $attempts flows ($ratio flows per host)! last seen scanning $ts_scan ($scan_flows flows), first attack started $ts_first";
                         syslog('info', $log);

                         # e-mail report iff host is not on whitelist and each victim is attacked by more than 3 flows (in average)
                         if (!Net::CIDR::cidrlookup($ip, @whitelist) && $ratio > 3 && !Net::CIDR::cidrlookup($ip, @blocked)) {

                                my $name_mail = '';
                                $name_mail = " ($name)" if ($name ne ''); # format domain name for e-mail

                                my $text = "Greetings,
                                
we would like to notify you that we blocked all incoming traffic
from the following IP address to the Masaryk University network
(147.251.0.0/16) becaus of this incident:

IP:       $ip$name_mail
Type:     SSH brute force attack
Severity: $attempts unsuccessful login attempts on TCP port 22
Time:     $ts_first $tz

This block will last for 14 days and is irrevocable.

Regards,
CSIRT-MU
http://www.muni.cz/csirt


Incident details:

Timestamp in $tz;Destination IP address;Number of attempts
\n";

                                my $query = "select ts, dip, flows from attacks where sip = '$ip' and ts >= '$ts_first' order by ts asc";
                                my $sth = $dbh->prepare($query);
                                $sth->execute();
                                while ((my $ts, my $dip, my $flows) = $sth->fetchrow_array()) {
                                        $text .= "$ts;$dip;$flows\n";
                                }
                                $sth->finish();

                                my $message = MIME::Lite->new 
                                (
                                        To      => $mail_to,
                                        From    => 'robot@nfsen-devel.ics.muni.cz',
                                        Subject => "SSH brute force attacks from $ip$name_mail",
                                        Data    => $text,
                                        'X-RT-Tool-Name' => 'BruteForceDetector',
                                        'X-RT-Attack-Type' => 'BruteForceAuth',
                                        'X-RT-Incident-IP' => $ip,
                                        'X-RT-BruteForceDetector-VictimCount' => $victim_count
                                );
                                $message->send('smtp', 'relay.ics.muni.cz');
                                if ($message->last_send_successful()) {
                                        syslog('info', "dict plugin: e-mail was sent successfully to $mail_to");
                                        $dbh->do("INSERT INTO blocked (ts,ip) values(now(), '$ip');") or syslog("info", "dict: Query failed:". $dbh->errstr);
                                } else {
                                        syslog('info', "dict plugin: error in sending e-mail to $mail_to");
                                }

                                # report to Warden server
                                # TODO: ts_first => detected 
                                my $time = DateTime->now();
                                $time->set_time_zone("Europe/Prague");
                                $time->set_time_zone("UTC");
                                my $detected = $time->strftime('%FT%T');

                                if (reportToWarden($ip, $detected, $victim_count)) {
                                        syslog('info', "dict plugin: reported to Warden server as a new event");
                                } else {
                                        syslog('info', "dict plugin: error in reporting to Warden server");
                                }
                         }
                }
        }

	syslog('info', "dict plugin: $i attackers inserted!");

        $dbh->disconnect();

	$total_time = sprintf ("%.6f", Time::HiRes::time - $total_time);
	syslog ('info', "dict run: TOTAL RUNTIME $total_time sec.");

        return;
}


sub trim {
	my $string = $_[0];
        $string =~ s/^\s+//;
	$string =~ s/\s+$//;
        return $string;
}

sub reportToWarden {
        # Path to warden-client folder
        my $warden_path = '/opt/warden-client';
               
        # Inclusion of warden-client sender module
        require $warden_path . '/lib/WardenClientSend.pm';

        my $source = shift;
        my $detected = shift;
        my $attack_scale = shift;

        my $service             = "SSHBruteForce-1_N";
        my $type                = "bruteforce";
        my $source_type         = "IP";
        my $target_proto        = "TCP";
        my $target_port         = "22";
        my $note                = "attack scale = # of attacked hosts";
        my $priority            = "null";
        my $timeout             = 60*24; # 1 day

        my @event = ($service, $detected, $type, $source_type, $source, $target_proto, $target_port, $attack_scale, $note, $priority, $timeout);

        # Sending event to Warden server - undefined return value
        WardenClientSend::saveNewEvent($warden_path, \@event);
        
        return 1;
}

#-------------------------------------------------------------------------------
#
# getCymruOrigin - obtaining origin (AS number) from whois.cymru.com 
#
#-------------------------------------------------------------------------------
sub getCymruOrigin {

        my $ip = shift;

        my $cymru = `whois -h whois.cymru.com "$ip"`;
        my @cymru_array = split("\n", $cymru);
        my @cymru_item = split(/\|/, $cymru_array[3]);
        my $origin = trim($cymru_item[0]);

        if ($origin =~ m/NA/) {
                $origin = "null";
        }

        return $origin;

} # End of getCymruOrigin


#
# Alert condition function.
# if defined it will be automatically listed as available plugin, when defining an alert.
# Called after flow filter is applied. Resulting flows stored in $alertflows file
# Should return 0 or 1 if condition is met or not
sub alert_condition {
	my $argref 		 = shift;

	my $alert 	   = $$argref{'alert'};
	my $alertflows = $$argref{'alertfile'};
	my $timeslot   = $$argref{'timeslot'};

	syslog('info', "Alert condition function called: alert: $alert, alertfile: $alertflows, timeslot: $timeslot");

	# add your code here

	return 1;
}

#
# Alert action function.
# if defined it will be automatically listed as available plugin, when defining an alert.
# Called when the trigger of an alert fires.
# Return value ignored
sub alert_action {
	my $argref 	 = shift;

	my $alert 	   = $$argref{'alert'};
	my $timeslot   = $$argref{'timeslot'};

	syslog('info', "Alert action function called: alert: $alert, timeslot: $timeslot");

	return 1;
}

#
# The Init function is called when the plugin is loaded. It's purpose is to give the plugin 
# the possibility to initialize itself. The plugin should return 1 for success or 0 for 
# failure. If the plugin fails to initialize, it's disabled and not used. Therefore, if
# you want to temporarily disable your plugin return 0 when Init is called.
#
sub Init {
        use NfConf;

	# Init some vars
	$nfdump  = "$NfConf::PREFIX/nfdump";
	$PROFILEDIR = "$NfConf::PROFILEDATADIR";

        # get plugin configuration from nfsen.conf
        my $conf = $NfConf::PluginConf{dict};
        # mail alert settings
        $mail_to = $$conf{'mail_to'};
        $channel_in = $$conf{'channel_in'};
        $channel_out = $$conf{'channel_out'};
        $db_host     = $$conf{'db_host'};
        $db_port     = $$conf{'db_port'};
        $db_name     = $$conf{'db_name'};
        $db_user     = $$conf{'db_user'};
        $db_passwd   = $$conf{'db_passwd'};

        my $whitelist_conf   = $$conf{'whitelist'};
        $whitelist_conf   =~ s/ //g;
        @whitelist   = split(/,/, $whitelist_conf);
        
        $tz = DateTime->now(time_zone => 'local');
        my $formatter = DateTime::Format::Strptime->new(pattern => "GMT%z");
        $tz->set_formatter($formatter);

	syslog("info", "dict plugin: Initialized: mail_to = $mail_to, channel_in = $channel_in, channel_out = $channel_out, whitelist = ".$$conf{'whitelist'});

	return 1;
}

#
# The Cleanup function is called, when nfsend terminates. It's purpose is to give the
# plugin the possibility to cleanup itself. It's return value is discard.
sub Cleanup {
	syslog("info", "dict plugin Cleanup");
}

1;
