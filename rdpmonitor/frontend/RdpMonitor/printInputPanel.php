<?php 
/**
 * RdpMonitor_PrintTimeInputPanel_attacks function is called from the place
 * in RdpMonitor_Run function where the Time input panel is to be printed. It prints
 * the HTML code of the panel. JavaScript code operating the panel is printed by 
 * RdpMonitor_PrintHeaders function.
 *
 * @param string $beginning Begin of interval in YYYY-MM-DD HH:MM format
 * @param string $ending End of interval in YYYY-MM-DD HH:MM format
 * @param string $timewindow Timewindow as a string (e.g. 5 minutes) 
 */
function  printInputPanel($type, $beginning, $ending, $timewindow) {

        // Should the input box be disabled when reloading the page? (i.e. is the "other" timewindow selected)?
        if("other" == $timewindow) { $begin_disabled = ""; } else { $begin_disabled = "disabled"; }

  ${"beginning".$type} = $beginning;
  ${"ending".$type} = $ending;
	// Prepare <option> tags with the timewindow sizes
        $options = array("other","6 hours","12 hours","1 day","1 week","1 month");
        $options_code = "";
        foreach ($options as $option) {
                if($option == $timewindow) {
                        $options_code .= "\t\t\t<option value=\"$option\" selected=\"selected\">$option</option>\n";
                } else {
                        $options_code .= "\t\t\t<option value=\"$option\">$option</option>\n";
                }
        }

        // Print panel itself
        print '

<form action="" id="time_form_'.$type.'" method="POST" onSubmit="document.getElementById(\'tab_'.$type.'\').style.opacity = 0.2;document.getElementById(\'tab_'.$type.'\').style.filter = \'alpha(opacity=20)\';">
<div class="time_select_panel">
  <table cellpadding="0" cellspacing="0" border="0">
    <tr>
       <td>
         Begin:<br>
         <input name="begin_datetime_'.$type.'" id="begin_datetime_'.$type.'" '.$begin_disabled.' type="text" size="16" value="'.${"beginning".$type}.'">
       </td>
       <td>
         <br><input type="button" id="prev_button_'.$type.'" value="&lt;" title="Select previous timewindow" style="width: 2em;">
       </td>
       <td>
         Timewindow:<br>
         <select name="timewindow_'.$type.'" id="timewindow_'.$type.'">'.$options_code.'</select>
       </td>
       <td>
         <br>
         <input type="button" id="next_button_'.$type.'" value="&gt;" title="Select next timewindow" style="width: 2em;">
       </td>
       <td>
         End:<br>
         <input name="end_datetime_'.$type.'" id="end_datetime_'.$type.'" type="text" size="15" value="'.${"ending".$type}.'">
       </td>
       <td>
         <div style="width: 30px;">&nbsp;</div>
       </td>
       <td>
         <br>
         <input type="submit" value="Show data for selected interval">
       </td>
       <td>
         <div style="width: 30px;">&nbsp;</div>
       </td>
    </tr>
    <tr>
      <td colspan=3>Source IP (optional):&nbsp;<input name="src_ip_'.$type.'" id="src_ip_'.$type.'" type="text" size="15" value=""></td>
      <td><div style="width: 30px;">&nbsp;</div></td>
      <td colspan=3>Destination IP(optional):&nbsp;<input name="dst_ip_'.$type.'" id="dst_ip_'.$type.'" type="text" size="15" value=""></td>
      <td colspan=2>Number of records(optional):</td><td><input name="limit_'.$type.'" id="limit_'.$type.'" type="number" size="4" value="100"></td>
      <td><div style="width: 30px;">&nbsp;</div></td>
    </tr>
  </table>
</div>
</form>';

}
?>
