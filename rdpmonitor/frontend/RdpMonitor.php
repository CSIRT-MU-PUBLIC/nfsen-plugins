<?php 

#PHP/JS headers
include 'plugins/RdpMonitor/headers.php';

#include PHP functions
#print InputPanel 
include 'plugins/RdpMonitor/printInputPanel.php';
#create highcharts
include 'plugins/RdpMonitor/Graphs/highchartAttacks.php';
include 'plugins/RdpMonitor/Graphs/highchartScans.php';
include 'plugins/RdpMonitor/Graphs/top10Victims.php';
include 'plugins/RdpMonitor/Graphs/top10Attackers.php';
include 'plugins/RdpMonitor/Graphs/highchartIpDetails.php';
include 'plugins/RdpMonitor/countryCodeToCountry.php';
include 'plugins/RdpMonitor/tryGetHost.php';

#STOP autorefresh
$_SESSION['refresh'] = 0;

/**
 * get_time_form_data is supporting function of timewindow selection panel
 * It checks if a form field given in argument was sent and returns its value. 
 * If it wasn't, default value is returned.
 */
function get_time_form_data($field_name, $default) {

        if (isset($_POST[$field_name])) {
                return $_POST[$field_name];
        } else {
                return $default;
        }
}

/* 
 * RdpMonitor_ParseInput is called prior to any output to the web browser 
 * and is intended for the plugin to parse possible form data. This 
 * function is called only, if this plugin is selected in the plugins tab. 
 * If required, this function may set any number of messages as a result 
 * of the argument parsing.
 * The return value is ignored.
 */
function RdpMonitor_ParseInput( $plugin_id ) {
} // End of RdpMonitor_ParseInput


/*
 * RdpMonitor_Run function is called after the header and the navigation bar have 
 * been sent to the browser. It's now up to this function what to display.
 * This function is called only, if this plugin is selected in the plugins tab
 * Its return value is ignored.
 */
function RdpMonitor_Run( $plugin_id ) {

//  $active = '#overview';

  RdpMonitor_PrintHeaders();
  $active = 'tab_overview';
// Print tabs container
  if(isset($_POST['save_email']) || isset($_POST['email'])){
    $active = '#tab_settings'; 
  }
  if(isset($_POST['end_datetime_attacks']) || isset($_POST['begin_datetime_attacks'])){
    $active = '#tab_attacks';
  }
  if(isset($_POST['end_datetime_scans'])||isset($_POST['begin_datetime_scans'])){
    $active = '#tab_scans'; 
  }
  //Get settings
    $opts['option'] = "";
    // Load current settings from backend
    $out_list = nfsend_query("RdpMonitor::load_settings", $opts);

    if(!is_array($out_list)){
       SetMessage('error', "Error calling plugin backend - load_settings");
       return FALSE;
    }

    $sqlite = $out_list[sqlite];

	// Get the current time and round it to the 5minutes
	$round_numerator = 60 * 5; // 60 seconds per minute * 5 minutes
	$current_rounded_time = ( round ( time() / $round_numerator ) * $round_numerator ); // Calculate time to nearest 5 minutes!
	$current_time = date("Y-m-d H:i", $current_rounded_time);

  // Initialization of time for Time Input Panel (set current time or time sent from the panel)
	$beginning_attacks = get_time_form_data("begin_datetime_attacks", date("Y-m-d H:i", $current_rounded_time -21600 ));
	$ending_attacks = get_time_form_data("end_datetime_attacks", date("Y-m-d H:i", $current_rounded_time));
	$timewindow_attacks = get_time_form_data("timewindow_attacks", "6 hours");
	$end_timestamp_attacks = strtotime("$ending_attacks");
	$begin_timestamp_attacks = strtotime("$beginning_attacks");
        $src_ip_attacks = $_POST['src_ip_attacks'];
        $dst_ip_attacks = $_POST['dst_ip_attacks'];
	$limit_attacks = $_POST['limit_attacks'];

	// Initialization of time for Time Input Panel (set current time or time sent from the panel)
	$beginning_scans = get_time_form_data("begin_datetime_scans", date("Y-m-d H:i", $current_rounded_time -21600 ));
	$ending_scans = get_time_form_data("end_datetime_scans", date("Y-m-d H:i", $current_rounded_time));
	$timewindow_scans = get_time_form_data("timewindow_scans", "6 hours");
	$end_timestamp_scans = strtotime("$ending_scans");
	$begin_timestamp_scans = strtotime("$beginning_scans");
        $src_ip_scans = $_POST['src_ip_scans'];
        $dst_ip_scans = $_POST['dst_ip_scans'];
	$limit_scans = $_POST['limit_scans'];

        // variable to collect dynamicaly generated Javascript code 
	$javascript_code = "";

	print '
  
        <script language="Javascript" type="text/javascript">
          $(document).ready(function() {
            $("#tab-container").easytabs({  animate: false, updateHash: false });
            $("#tab-container").easytabs("select", "'.$active.'");
          });
        </script>

	<div id="tab-container" class="tab-container">
	<ul class="menu">
		<li class="tab"><a href="#tab_overview">Overview</a></li>
		<li class="tab"><a href="#tab_attacks">Attacks</a></li>
		<li class="tab"><a href="#tab_scans">Scans</a></li>
		<li class="tab"><a href="#tab_settings">Settings</a></li>
		<li class="tab"><a href="#tab_about">About</a></li>
	</ul>';
	print '	<div class="panel-container">';
  
  // ======================================================= TAB Overview

	print '		<div id="tab_overview" style="min-width: 1600px;"><br>';
   
	// options for calling backend function using selected timewindow
	// Name of the graph for the backend to know, which data should be sent
  $opts["begin"] = date("U", $current_rounded_time - 86400);
  $opts["end"] = date("U", $current_rounded_time);
  $opts["graph_name"] = "highchart_scans";
  $opts["render_to"] = "highchart-overview-scans";
  $javascript_code .= RdpMonitor_get_highchart_scans($opts);

  $opts["begin"] = date("U", $current_rounded_time - 86400);
  $opts["end"] = date("U", $current_rounded_time);
  $opts["graph_name"] = "highchart_attacks";
  $opts["render_to"] = "highchart-overview-attacks";
  $javascript_code .= RdpMonitor_get_highchart_attacks($opts);
  
  $opts['graph_name'] = "highchart_top10_victims";
  $javascript_code .= RdpMonitor_get_highchart_top10_victims($opts);

  if(!$sqlite){
    $opts["graph_name"] = "highchart_ip_details";
    $javascript_code .= RdpMonitor_get_highchart_ip_details($opts);
  }

  $opts['graph_name'] = "highchart_top10_attackers";
  $javascript_code .= RdpMonitor_get_highchart_top10_attackers($opts);

  
  print '
  <table>
    <tr>
      <td><div id="highchart-overview-scans" class="highchart_graph ui-corner-all" style="width: 700px;"></div></td>
      <td><div id="highchart-overview-attacks" class="highchart_graph ui-corner-all" style="width: 700px;"></div></td>
    </tr>
    <tr>
      <td><div id="highchart-top10_victims" class="highchart_graph ui-corner-all"></div></td>
      <td><div id="highchart-top10_attackers" class="highchart_graph ui-corner-all"></div></td>
    </tr>';
   if(!$sqlite){
    print '
    <tr>
     <td colspan=2><div id="highchart-ip_details" class="highchart_graph ui-corner-all"></div></td>
    </tr>';
   }
   print '</table>';
   

  print '   </div><!-- End of TAB overview -->';

	// ======================================================= TAB Attacks

	print '		<div id="tab_attacks" name="tab_attacks"><br>';


	// Print HTML code of the TimeInputPanel
	printInputPanel('attacks', $beginning_attacks, $ending_attacks, $timewindow_attacks);
	// options for calling backend function using selected timewindow
        $opts['begin'] = "$begin_timestamp_attacks";
        $opts['end'] = "$end_timestamp_attacks";
        $opts['window'] = "$timewindow_attacks";
	$opts['src_ip'] = "$src_ip_attacks";
	$opts['dst_ip'] = "$dst_ip_attacks";
	$opts['limit'] = "$limit_attacks";
        $opts['render_to'] = "highchart-attacks";
	// Name of the graph for the backend to know, which data should be sent
	$opts["graph_name"] = "highchart_attacks";
	print '<div id="highchart-attacks" class="highchart_graph ui-corner-all"></div>';
	$javascript_code .= RdpMonitor_get_highchart_attacks($opts);

	$opts['option'] = "";
	    
	// call command in backened plugin
	$out_list = nfsend_query("RdpMonitor::get_pg_data", $opts);
	// get result
	//if $out_list == FALSE – it's an error
	if ( !is_array($out_list) ) {
            SetMessage('error', "Error calling plugin");
	//    return FALSE;
	}

#	$timestamp = $out_list['timestamp'];
#	$http_requests = $out_list['http_requests'];
        $attacks = $out_list['attacks'];	
	if(!isset($_POST['limit_attacks'])){
          $limit_attacks = 100;
	}
	print "<h3 style=\"margin-left: 30px;\">Authentication attempts stored in the database sorted by timestamp (first ".$limit_attacks." records)</h3>";
	// check the correct number of received strings
	if (count($attacks) < 1) {
                print "<table class=\"data_table\" cellpadding=\"0\" cellspacing=\"0\">";
                print "<tr style=\"background-color: #cedfda;\"><td><b>Nothing to display! Backend returned empty list.</b></td></tr>";
	} else {
                print '<table class="tablesorter" cellpadding="0" cellspacing="0">';
                print '<thead title="Press SHIFT and click additional column to sort by multiple columns"><tr><th>Timestamp</th><th>Source IP</th><th>Name</th><th>Country</th><th>Destination IP</th><th>Name</th><th>Flows</th></tr></thead><tbody>';
 

	for ($i = 0; $i < sizeof($attacks); $i= $i+6) {
      	        print "<tr>";
                        print "<td>".$attacks[$i]."</td>";
                        print '<td class="ip" onclick="document.getElementById(\'src_ip_attacks\').value=this.innerHTML">'.$attacks[$i+1]."</td>";
                        print "<td>".$attacks[$i+2]."</td>";
                        print "<td>".$attacks[$i+3]."</td>";
                        print '<td class="ip" onclick="document.getElementById(\'dst_ip_attacks\').value=this.innerHTML">'.$attacks[$i+4]."</td>";
                        print "<td>".tryGetHost($attacks[$i+4])."</td>";
                        print "<td>".$attacks[$i+5]."</td>";
          			print "</tr>";
		}
	}
	print "</tbody></table>";


	print '		</div><!-- End of TAB Attacks -->';

	// ======================================================= TAB Scans

	print '		<div id="tab_scans"><br>';


	// Print HTML code of the TimeInputPanel

	printInputPanel('scans', $beginning_scans, $ending_scans, $timewindow_scans);
	// options for calling backend function using selected timewindow
        $opts['begin'] = "$begin_timestamp_scans";
        $opts['end'] = "$end_timestamp_scans";
        $opts['window'] = "$timewindow_scans"; 
	$opts['src_ip'] = "$src_ip_scans";
	$opts['dst_ip'] = "$dst_ip_scans";
	$opts['limit'] = "$limit_scans";
        $opts['render_to'] = "highchart-scans";
	// Name of the graph for the backend to know, which data should be sent
	$opts["graph_name"] = "highchart_scans";
	print '<div id="highchart-scans" class="highchart_graph ui-corner-all"></div>';
	$javascript_code .= RdpMonitor_get_highchart_scans($opts);
	// Name of the graph for the backend to know, which data should be sent

	$opts['option'] = "";
	    
	// call command in backened plugin
	$out_list = nfsend_query("RdpMonitor::get_pg_data", $opts);

	// get result
	// if $out_list == FALSE – it's an error
	if ( !is_array($out_list) ) {
          SetMessage('error', "Error calling plugin");
          return FALSE;
	}

        $scans = $out_list['scans'];	
	if(!isset($_POST['limit_scans'])){
          $limit_scans = 100;
	}

	print "<h3 style=\"margin-left: 30px;\">Scans stored in the database sorted by timestamp (first ".$limit_scans." records)</h3>";
	// check the correct number of received strings
	if (count($scans)<1) {
                print "<table class=\"data_table\" cellpadding=\"0\" cellspacing=\"0\">";
                print "<tr style=\"background-color: #cedfda;\"><td><b>Nothing to display! Backend returned empty list.</b></td></tr>";
	} else {
                print '<table class="tablesorter" cellpadding="0" cellspacing="0">';
                print '<thead title="Press SHIFT and click additional column to sort by multiple columns"><tr><th>Timestamp</th><th>Source IP</th><th>Name</th><th>Country</th><th>Destination IP</th><th>Name</th><th>Flows</th></tr></thead><tbody>';
	for ($i = 0; $i < sizeof($scans); $i= $i+6) {
	            	print "<tr>";
                        print "<td>".$scans[$i]."</td>";
                        print '<td class="ip" onclick="document.getElementById(\'src_ip_scans\').value=this.innerHTML">'.$scans[$i+1]."</td>";
                        print "<td>".$scans[$i+2]."</td>";
                        print "<td>".$scans[$i+3]."</td>";
                        print '<td class="ip" onclick="document.getElementById(\'dst_ip_scans\').value=this.innerHTML">'.$scans[$i+4]."</td>";
                        print "<td>".tryGetHost($scans[$i+4])."</td>";
                        print "<td>".$scans[$i+5]."</td>";
          			print "</tr>";
		}
	}
	print "</tbody></table>";


print '		</div><!-- End of TAB Scans -->';

// ======================================================= TAB Settings
print '		<div id="tab_settings"><br>';
	
  include 'plugins/RdpMonitor/settings.php';

print '		</div><!-- End of TAB Settings -->';
// ======================================================= TAB About
print '		<div id="tab_about"><br>';
	
  include 'plugins/RdpMonitor/about.html';

print '		</div><!-- End of TAB About --></div>';


// ======================================================= footer
print '
  <div style="color: #666666; border: 1px solid white; font-size: 9pt; text-align: right;
    padding-bottom: 20px; width: 95%;">
    <img src="plugins/RdpMonitor/css/images/logo_mu.gif" width="50" hspace="0" style="vertical-align: bottom; opacity: 0.5; float: right;">
    <div style="margin-top: 8px; padding-right: 55px;">
      &#169 2012, Masaryk University, RdpMonitor Plugin<br>
    </div>
  </div>
</div>';


// ======================================================= Print all generated JavaScript code (for drawing graphs)
  print '<script>';
    print $javascript_code;
  print '</script>';
} // End of RdpMonitor_Run

