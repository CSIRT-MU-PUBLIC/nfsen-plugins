#!/usr/bin/perl

#
# fillDB.pm -  script for initialization of database 
#
# Copyright (C) 2012 Masaryk University
# Author: Martin Vizvary <vizvary@ics.muni.cz>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in
#    the documentation and/or other materials provided with the
#    distribution.
# 3. Neither the name of the Masaryk University nor the names of its
#    contributors may be used to endorse or promote products derived from
#     this software without specific prior written permission.
#
# This software is provided ``as is'', and any express or implied
# warranties, including, but not limited to, the implied warranties of
# merchantability and fitness for a particular purpose are disclaimed.
# In no event shall the Masaryk University or contributors be liable for
# any direct, indirect, incidental, special, exemplary, or consequential
# damages (including, but not limited to, procurement of substitute
# goods or services; loss of use, data, or profits; or business
# interruption) however caused and on any theory of liability, whether
# in contract, strict liability, or tort (including negligence or
# otherwise) arising in any way out of the use of this software, even
# if advised of the possibility of such damage.
#
# 1.0.0
#


use strict;
use warnings;

use Getopt::Long;
use DBD::Pg;
use DBI;
use DateTime;
use IP::Country::Fast;
use Socket;
use Time::HiRes;

my ($DB_HOST, $DB_NAME, $DB_USER, $DB_PASS, $DB_PORT, $NFPROF, $NETWORK, $NFDUMP);

GetOptions(
   'dbhost=s'      => \$DB_HOST,
   'dbname=s'      => \$DB_NAME,
   'dbuser=s'      => \$DB_USER,
   'dbpass=s'      => \$DB_PASS,
   'dbport=s'      => \$DB_PORT,
   'nfprofile=s'   => \$NFPROF,
   'network=s'     => \$NETWORK,
   'nfdump=s'      => \$NFDUMP
);

=kom
  usage - prints help and exits
=cut
sub usage
{
print '
 fillDB script -- initialization of database
 Arguments:
  --dbhost - database hostname/ip address
  --dbname - database name
  --dbuser - database user
  --dbpass - database password
  --dbport - database port
  --nfprofile - NetFlow profile-data path
  --network - local network CIDR format
  --nfdump - path to nfdump'."\n\n";
  exit(0);
}

if(!defined $DB_HOST){
  print STDERR "dbhost parameter is missing\n";
  usage();
  exit(1);
}
if(!defined $DB_NAME){
  print STDERR "dbname parameter is missing\n";
  usage();
  exit(1);
}
if(!defined $DB_USER){
  print STDERR "dbuser parameter is missing\n";
  usage();
  exit(1);
}
if(!defined $DB_PASS){
  print STDERR "dbpass parameter is missing\n";
  usage();
  exit(1);
}
if(!defined $DB_PORT){
  print STDERR "dbport parameter is missing\n";
  usage();
  exit(1);
}
if(!defined $NFPROF){
  print STDERR "nfprofile parameter is missing\n";
  usage();
  exit(1);
}
if(!defined $NETWORK){
  print STDERR "network parameter is missing\n";
  usage();
  exit(1);
}
if(!defined $NFDUMP){
  print STDERR "nfdump parameter is missing\n";
  usage();
  exit(1);
}


my $start = DateTime->now(time_zone => 'local')->subtract( days=>7)->truncate(to => 'hour');
my $next = $start->clone();
$start->add(minutes => 5);

print time()."\n";
print "RdpMonitor fillDB started\n " . time();
my $total_time = Time::HiRes::time();
for(my $i=0; $i<7;$i++){
  $next->add(days => 1);
  my $day_time = Time::HiRes::time();
  
  my ($rc, $res) = run($start,$next);
  if(!$rc){
    print $res."\n";
  }
  $day_time = sprintf("%.6f", Time::HiRes::time() - $day_time);
  print "fillDB: interval $start - $next; RUNTIME $day_time\n";
}

$total_time = sprintf("%.6f", Time::HiRes::time() - $total_time);
print "fillDB: TOTAL RUNTIME $total_time sec.\n";

sub run
{
  my $start = shift;
  my $next = shift;

  print "Working on timeslot " . $start->strftime('%Y%m%d%H%M'). "--" . $next->strftime('%Y%m%d%H%M')."\n";

  # Convert given timeslot into unix timestamp format
  my $input_file_start    = $start->ymd('/')."/nfcapd.". $start->strftime('%Y%m%d%H%M');
  my $input_file_next     = $next->ymd('/')."/nfcapd.".$next->strftime('%Y%m%d%H%M');

  my $dsn = "dbi:Pg:database=$DB_NAME;host=$DB_HOST;port=$DB_PORT";
  my $dbh = DBI->connect($dsn, $DB_USER, $DB_PASS) or return(0, "Cannot connect to database: $dsn " . $DBI::errstr.", $DB_NAME, $DB_HOST, $DB_PORT");

  ## find all scanners
  my $nf_filter_scan = 'proto tcp and dst port 3389 and flags S and not flags ARFUP and net ' . $NETWORK;
  while($input_file_start ne $input_file_next){

my $time = Time::HiRes::time();
    my @ips = `$NFDUMP -M $NFPROF -T -r $input_file_start '$nf_filter_scan' -o 'fmt:%ts;%sa;%da;%fl' -a -A srcip,dstip -q` or return(0, "FillDB get nfdump scans failed $@");
    if(!@ips){
      print "no scans $@";
      exit(1);
    } 
    my $sthScans = $dbh->prepare("INSERT INTO scans VALUES (?,?,?,?);");
    my $i = 0;
    # optimalizace -o fmt:insert into...
    for ($i = 0; $i < @ips; $i++){
      my @flow = split(/;/,$ips[$i]);
      my $ts = $flow[0];
      my $sip = trim(substr($flow[1],1));
      my $dip = trim(substr($flow[2],1));
      my $flows = trim(substr($flow[3],1));
      $sthScans->execute($ts,$sip,$dip,$flows) or return(0, "RdpMonitor: run: Cannot execute query " . $dbh->errstr);
    }
    print "Scans OK\n";

  my $bi_filter = 'in packets >20 and in packets < 100 and out packets > 30 and out packets < 190 and in bytes > 2200 and in bytes < 8001 and out bytes > 3000 and out bytes < 180000 and flags APRS and dst net '. $NETWORK .' and dst port 3389 and proto tcp';
  # attacks
  #make biflow from traffic
  `$NFDUMP -M $NFPROF -T -r $input_file_start -B -w /tmp/out_ips_fill -m 'net $NETWORK and port 3389 and proto tcp'`;
  #apply filter on biflow
  my @out_ips = `$NFDUMP -r /tmp/out_ips_fill -m '$bi_filter' -o "fmt:%ts;%sa;%da;%fl" -q -m -A srcip,dstip`;

  my $reg = IP::Country::Fast->new();

  ##for each attack ip
  my $sthDetails = $dbh->prepare("INSERT INTO ip_details VALUES(?,?,?,?,?)");
  my $sthAttacks = $dbh->prepare("INSERT INTO attacks VALUES(?,?,?,?)");

  for ($i = 0; $i < @out_ips; $i++){
    my @flow = split(/;/,$out_ips[$i]);
    my $ts = $flow[0];
    my $sip = trim(substr($flow[1],1));
    my $dip = trim(substr($flow[2],1));
    my $fl = trim(substr($flow[3],1));

    my $name = scalar gethostbyaddr(inet_aton($sip), AF_INET);
    my $asn = getCymruOrigin($sip);
    my $country = $reg->inet_atocc($sip);
   
    $sthDetails->execute($ts,$sip,$name,$asn,$country) or warn("Query execution failed: $sthDetails; args: $ts,$sip,$name,$asn,$country; $DBI::errstr");

    $sthAttacks->execute($ts,$sip,$dip,$fl) or warn("Query execution failed: $sthAttacks; args: $ts,$sip,$dip,$fl; $DBI::errstr");
  }
    print  "Attacks OK\n";
    $start->add(minutes => 5);
    $input_file_start    = $start->ymd('/')."/nfcapd.". $start->strftime('%Y%m%d%H%M');
  print "$input_file_start\n";
  $time = sprintf("%.6f", Time::HiRes::time() - $time);
  print "RdpMonitor run: RUNTIME $time sec.\n";
  }
  # end of "run" function print the running time into syslog
$dbh->disconnect() or warn "cannot disconnect";
  return(1);
} # End of run

=item
 getCymruOrigin - obtain origin (AS number) from whois.cymru.com

 Input: IP address
 Output:
   On success: (1, 'origin')
   On error:   (0, 'error message')
=cut
sub getCymruOrigin
{
  my $ip = shift;

  my $cymru = `whois -h whois.cymru.com "$ip"`;

  my @cymru_array = split("\n", $cymru);
  my @cymru_item = split(/\|/, $cymru_array[3]);
  my $origin = trim($cymru_item[0]);

  if ($origin =~ m/NA/){
    $origin = "null";
  }

  return (1, $origin);

} # End of getCymruOrigin

=item
  trim - remove spaces from in front of and behind the string

  Input: 'string'
  Ouptu: 'trimmed string'
=cut
# trim spaces from string
sub trim
{
  my $string = $_[0];
  $string =~ s/^\s+//;
  $string =~ s/\s+$//;
  return($string);
}

