* uvodni stranka

RdpMonitor

RdpMonitor je plugin ur�en� pro NetFlow kolektor <a href="http://nfsen.sourceforge.net/">NfSen</a>, kter� detekuje �toky na autentizaci Remote Desktop Protocol (RDP), na z�klad� sesb�ran�ch NetFlow dat.

<a href="">Vice o RdpMonitoru</a>


Download

   <a href="">RdpMonitor (verze 1.0.1) 
   <a href="">Kontroln� sou�et (SHA-1) bal��ku,
   <a href="">README</a>
   Datum vyd�n�: 7. 1. 2013

Kontakt

V p��pad� dotaz� ohledn� pluginu kontaktujte v�vojov� t�m na e-mailov� adrese 
csirt-info(at)muni.cz
 


========================

* v�ce o RdpMonitoru

V�ce o RdpMonitoru

RdpMonitor je plugin ur�en� pro NetFlow kolektor <a href="http://nfsen.sourceforge.net/">NfSen</a>, kter� detekuje �toky na autentizaci protokolu vzd�len� plochy -- Remote Desktop Protocol (RDP), na z�klad� sesb�ran�ch NetFlow dat. RdpMonitor spracovan� data ukl�d� a ohla�uje detekovan� �toky, kter� ohro�uj� chr�n�nou s�. Plugin podporuje protokol IPv4.

Detekce je zalo�ena na NetFlow datech z�skan�ch ze s�t�, pomoc� n�stroje <a href="http://nfdump.sourceforge.net/">nfdump</a> aplikac� filtru. N�sledn� plugin analyzuje sesb�ran� data a vyhodnot� je-li dan� tok sou��st� �toku nebo ne.

RdpMonitor nab�z� webov� rozhran�, kter� je sou��st� kolektoru <a href="http://nfsen.sourceforge.net/">NfSen</a> a zobrazuje sesb�ran� data a statistiky detekce. Rozhran� umo��uje jednoduchou editaci textu e-mailu, kter� slou�� k hl�en� �tok�. 

==========================

�vodn� obrazovka webov�ho rozhran� RdpMonitoru nab�z�:

<image>overview.png</image>

 * grafy v�voje detekce za posledn�ch 24 hodin
 * deset nejaktivn�j��ch �to�n�k� a nejv�ce napaden�ch za��zen�
 * p�ehled rozlo�en� �to�n�k� podle krajiny p�vodu za posledn�ch 6 m�s�c�

Z�lo�ky Attacks a Scans zobrazuj�:

<image>attacks.png</image>

* graf po�tu �tok�/�to�n�k� ve zvolen�m �asov�m okne
* detailn� v�pis ulo�en�ch anom�li�   

N�sleduj�c� z�lo�ky Settings a About poskytuj� informace o nastaven� a informace o pluginu samotn�m. 

   
