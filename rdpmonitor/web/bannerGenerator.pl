#!/usr/bin/perl

use DBD::Pg;
use DBI;
use GD;

my $templatedir = "/home/flowmon/exec_scripts/RdpMonitor-banner";
my $exportdir = "/var/www/html/public/RdpMonitor-banner";
my $dbname = "rdpmonitoring";
my $dbhost = "147.251.14.42";
my $dbport = "5432";
my $dbuser = "rdpmonitoring";
my $dbpasswd = "rdpm0n1t0r1ng";


my $dbh = DBI->connect("DBI:Pg:dbname=$dbname;host=$dbhost;port=$dbport", $dbuser, $dbpasswd);
if ( !defined $dbh ) { die "Cannot connect to database"; }

$query = $dbh->prepare("select sum(flows) from attacks where ts > ( current_timestamp - interval '1 day' );");
$query->execute;
@row = $query->fetchrow;
$flows = $row[0];

$query = $dbh->prepare("select sum(flows) from attacks where ts > ( current_timestamp - interval '1 week' );");
$query->execute;
@row = $query->fetchrow;
$wflows = $row[0];

$query = $dbh->prepare("select count(distinct sip) from attacks where ts > ( current_timestamp - interval '1 day' );");
$query->execute;
@row = $query->fetchrow;
$ips = $row[0];

$query = $dbh->prepare("select count(distinct sip) from attacks where ts > ( current_timestamp - interval '1 week' );");
$query->execute;
@row = $query->fetchrow;
$wips = $row[0];

$query->finish;
$dbh->disconnect();

#cz
my $cz = GD::Image->newFromGif("$templatedir/RdpMonitor-v3-bianco-day-cz.gif");
my $blue = $cz->colorAllocate(19,43,112);
$cz->string(gdGiantFont, 58, 64, "$ips", $blue);
$cz->string(gdGiantFont, 58, 121, "$flows", $blue);

my $wcz = GD::Image->newFromGif("$templatedir/RdpMonitor-v3-bianco-week-cz.gif");
my $blue = $wcz->colorAllocate(19,43,112);
$wcz->string(gdGiantFont, 58, 64, "$wips", $blue);
$wcz->string(gdGiantFont, 58, 121, "$wflows", $blue);

my $czgif = $cz->gifanimbegin(1, 0);
$czgif   .= $wcz->gifanimadd(0, 0, 0, 500);
$czgif   .= $cz->gifanimadd(0, 0, 0, 500);
$czgif   .= $cz->gifanimend;

open OUTPUT, ">", "$exportdir/banner-cz.gif";
binmode OUTPUT;
print OUTPUT $czgif;
close OUTPUT;

#en
my $en = GD::Image->newFromGif("$templatedir/RdpMonitor-v3-bianco-day-en.gif");
my $blue = $en->colorAllocate(19,43,112);
$en->string(gdGiantFont, 58, 64, "$ips", $blue);
$en->string(gdGiantFont, 58, 121, "$flows", $blue);

my $wen = GD::Image->newFromGif("$templatedir/RdpMonitor-v3-bianco-week-en.gif");
my $blue = $wen->colorAllocate(19,43,112);
$wen->string(gdGiantFont, 58, 64, "$wips", $blue);
$wen->string(gdGiantFont, 58, 121, "$wflows", $blue);

my $engif = $en->gifanimbegin(1, 0);
$engif   .= $wen->gifanimadd(0, 0, 0, 500);
$engif   .= $en->gifanimadd(0, 0, 0, 500);
$engif   .= $en->gifanimend;

open OUTPUT, ">", "$exportdir/banner-en.gif";
binmode OUTPUT;
print OUTPUT $engif;
close OUTPUT;
