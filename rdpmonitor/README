+---------------------------------+
| README - NfSen RdpMonitor Plugin |
+---------------------------------+

Author            Martin Vizvary, vizvary@ics.muni.cz
                  Jan Vykopal, vykopal@ics.muni.cz
Version           1.1.1
Release date      2013-06-18

RdpMonitor - Remote Desktop Protocol monitoring plugin for NfSen provides
information about RDP activity in network based on NetFlow data. 
RdpMonitor detects brute force and dictionary attacks on host with open TCP
port 3389. The plugin reports attackers that threatens local network. Web
interface present captured flows and statistics about recent network activity.

Content:
========
1. License
2. Requirements
3. Installation
4. Configuration
  4.1 Database settings
    4.1.1 PostgreSQL database settings
    4.1.2 SQLite database settings
  4.2 Database initialization
  4.3 Creating NfSen profile
  4.4 Plugin configuration
  4.5 Editing e-mail messages
  4.6 Reload of NfSen
5. Using the RdpMonitor plugin interface
6. Uninstallation
  6.1 Uninstallation of RdpMonitor plugin
  6.2 Uninstallation of database


1. License:
===========
  * The plugin frontend includes the third party software component JQuery,
    distributed under the MIT License, Highcharts under Creative Commons 
    Attribution-NonCommercial 3.0 License.
  * The plugin backend uses the third party software component PostgreSQL database.
  * The plugin backend and frontend code is available under the BSD License.


2. Requirements:
================
  * Perl 5.8.8 or higher  
  * PHP 5.5.3 or higher
  * PostgreSQL 8.4.9 or higher or SQLite 3.3.6 or higher
  * NfSen 1.3.6 or higher
  * nfdump 1.6.5 or higher
  * Perl modules:
    * DBD::Pg
    * DBI
    * DateTime
    * Email::Simple
    * IP::Country::Fast
    * Net::CIDR
    * POSIX
    * Sys::Syslog
    * Socket
    * URI::Escape


3. Installation:
================

1. Unpack RdpMonitor tarball.

    $ tar xzvf rdpmonitor-1.1.1.tar.gz

Copy the content of backend directory to $BACKEND_PLUGINDIR
the content of frontend directory to $FRONTEND_PLUGINDIR
 
    $ cp -R ./backend/* $BACKEND_PLUGINDIR 
    $ cp -R ./frontend/* $FRONTEND_PLUGINDIR

$BACKEND_PLUGINDIR and $FRONTEND_PLUGINDIR are specified in /data/nfsen/etc/nfsen.conf.

Make sure that RdpMonitor will be able to create and write to files in $BACKEND_PLUGINDIR/RdpMonitor/


4. Configuration:
=================

4.1 Database settings
Plugin supports PostgreSQL and SQLite databases. For the long run, we recommend using PostgreSQL database due
to SQLite performance.

4.1.1 PostgreSQL database settings
----------------------------------
Assuming the PostgreSQL database is installed and running
log in as database admin

    $ su -
    # su - postgres
    $ psql

Create the database

    =# CREATE DATABASE rdpmonitordb;

Create new database user with <password>

    =# CREATE USER rdpmonitor WITH PASSWORD '<password>';

Grant all privileges on RdpMonitor database to this user

    =# GRANT ALL ON DATABASE rdpmonitordb TO rdpmonitor;

Exit PostgreSQL console

    =# \q
    
Create tables
  
    $ psql -U rdpmonitor -d rdpmonitordb < ./backedn/PostgreSQL-init

Tables and indexes will be created at first initialization.

4.1.2 SQLite database settings
------------------------------

Use the file with DB schema from package (RdpMonitor-1.1.1/backend/RdpMonitor/rdpmonitoring). 
User running nfsen must have granted RW access to the database. We recommend to use sqlite only for testing
for several weeks. When the database file grows to the several hundreds MBs, the frontend is unstable.


4.2 Database initialization (only PostgreSQL)
---------------------------------------------
This step is optional. The detection will start faster if you 
initialize database with scan and attack data. This step will take several hours
depends on stored netflow data. 

  --dbhost <database hostname/ip address>
  --dbname <database name>
  --dbuser <database user>
  --dbpass <database password>
  --dbport <database port>
  --nfprofile <NetFlow profile-data path>
  --network <local network CIDR format>
  --nfdump <path to nfdump>

$ ./fillDB.pm --dbhost <database host> --dbname <database name>
 --dbuser <database user> --dbpass <database password> --dbport <database port>
 --nfprofile <path to profile data> --network <local network CIDR> 
 --nfdump <path to nfdump>

4.3 Creating NfSen profile
--------------------------
Plugin needs specific profile for RDP traffic. Name of the profile have to be the same
as in nfsen.conf specified in 4.4. 
How to create a NfSen profile and channels: http://nfsen.sourceforge.net/#mozTocId623518

Profile must contain three channels:
  * in - RDP related traffic incomming to local network
       - filter: proto tcp and port 3389 and dst net <local network address>  
  * out - RDP related traffic outgoing from local network
        - filter: proto tcp and port 3389 and src net <local network address>
  * in-scan - scans on port 3389 incomming to local network
            - filter: port 3389 and proto tcp and dst net <local network address>
                      and flags S and not flags ARPUF

4.4 Plugin Configuration
------------------------
Add the following line to the @plugins section of nfsen.conf

  [ '<profile-name>', 'RdpMonitor' ],

Modify %PluginConf hash variable in nfsen.conf:

Plugin configuration using SQLite:

%PluginConf = (
  ...
  RdpMonitor => {
    sqlite           => '1',
    sqlite_path      => '<path_to_sqlite_db_file>',
    mail_to          => 'operator@your.domain',
    mail_from        => 'RdpMonitor@your.domain',
    channel_in       => 'in',
    channel_out      => 'out',
    channel_scan     => 'in-scan',
    local_network    => '<local network address>',
    whitelist        => '<whitelist>',
  },
  ...
);

Plugin configuration using PostgreSQL:

%PluginConf = (
  ...
  RdpMonitor => {
    sqlite           => '0',
    db_name          => '<dbname>',
    db_host          => '<dbhost>',
    db_port          => '<dbport>',
    db_user          => '<dbuser>',
    db_passwd        => '<dbpasswd>',
    mail_to          => 'operator@your.domain',
    mail_from        => 'RdpMonitor@your.domain',
    channel_in       => 'in',
    channel_out      => 'out',
    channel_scan     => 'in-scan',
    local_network    => '<local network address>',
    whitelist        => '<whitelist>',
  },
  ...
);

 - <sqlite>          0 - using PostgreSQL, 1 - using SQLite

 - SQLite related config part:
   - <sqlite_path>     path to the SQLite database file (/data/nfsen/plugins/RdpMonitor/rdpmonitoring) (mandatory if using sqlite)

 - PostgreSQL related config part (mandatory for PostgreSQL):
   - <db_name>       name of the database specified in 4.1 ("rdpmonitordb")
   - <db_host>       hostname of remote database host or "localhost" if the database runs locally
   - <db_port>       database port (usually 5432)
   - <db_user>       database user created in 4.1 ("rdpmonitor")
   - <db_passwd>     password for the database user created in 4.1 ("password")

 - <mail_to>         e-mail address that will receive all the notifications
 - <mail_from>       sender's address
 - in                NfSen channel for RDP communication in local network
 - out               NfSen channel for RDP communication from local network
 - in_scan           NfSen channel for RDP scan communication in local network
 - <local_network>   local network range in CIDR format
 - <whitelist>       comma separated list of networks (full net, not e. g. 192.168/16)


4.5 Editing e-mail messages
---------------------------
You can edit the text of e-mail message from Frontend part of plugin. E-mail includes short description
of exported variables you can use in your text (e.g., reported IP, current time, etc.).


4.6 Reload of NfSen
-------------------
    
    $BINDIR/nfsen reload


5. Using RdpMonitor plugin interface:
==========================

RdpMonitor interface is accesible via NfSen web interface in the 'Plugins' section.
First you see 'Overview' tab containing graphs of most active suspicious addresses, victims,
attacks and scans statistics. You can see trends from which country the most attackers
come from. Tabs 'Attacks' and 'Scans' are supposed to present data for network analysis
from captured flows. 'Settings' tab allows user to change e-mail content and revise
settings in nfsen.conf.

6. Uninstallation:
==================

6.1 Uninstallation of RdpMonitor plugin
--------------------------------------
Stop NfSen
    
    $BINDIR/nfsen stop

Edit file nfsen.conf - delete line

  [ 'rdp', 'RdpMonitor' ],

in @plugins section.
Delete hash variable entry

  RdpMonitor => {
    ...
  }

in section %PluginConf.

Remove files and directories

    $ rm -rf $BACKEND_PLUGINDIR/RdpMonitor*
    $ rm -rf $FRONTEND_PLUGINDIR/RdpMonitor*

6.2 Uninstallation of database
------------------------------

PostgreSQL:
Connect to database as admin, delete RdpMonitor database in PostgreSQL console:

    $ su -
    # su - postgres
    $ psql

    =# DROP DATABASE rdpmonitordb
    =# \q

SQLite:
Delete the database file:
  $ rm <path_to_sqlite_db_file>
